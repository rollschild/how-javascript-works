# How Code Generation Works

- The next step is to take the tree that the parser wove and transform it into
  an executable form
  - **code generation**
  - but really a transformation
- Most of the code generator is just some simple functions that convert
  tokens back into text
