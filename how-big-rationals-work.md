# How Big Rationals Work

- A rational number is a number that can be expressed as the ratio of two
  integers
- A rational system is concerned with two numbers:
  - **numerator**
  - **denominator**

```math
value = numerator / denominator
```

- rational values in our big rationals system are objects with `numerator` and
  `denominator` properties that are both big integers
