function integer(from = 0, to = Number.MAX_SAFE_INTEGER, step = 1) {
  return function () {
    if (from < to) {
      const result = from;
      from += step;
      return result;
    }
  };
}

function element(array, gen = integer(0, array.length)) {
  return function elementGenerator(...args) {
    const element = gen(...args);
    if (element !== undefined && element < array.length) {
      return array[element];
    }
  };
}

function property(object, gen = element(Object.keys(object))) {
  return function propertyGenerator(...args) {
    const key = gen(...args);
    if (key !== undefined) {
      return [key, object[key]];
    }
  };
}

function collect(generator, array) {
  return function collectGenerator(...args) {
    const value = generator(...args);
    if (value !== undefined) {
      array.push(value);
    }
    return value;
  };
}

function repeat(generator) {
  if (generator() !== undefined) {
    return repeat(generator);
  }
}

const array = [];
repeat(collect(integer(0, 9), array));

function harvest(generator) {
  const array = [];
  repeat(collect(generator, array));
  return array;
}

function filter(generator, predicate) {
  return function filterGenerator(...args) {
    const value = generator(...args);
    if (value !== undefined && !predicate(value)) {
      return filterGenerator(...args);
    }
    return value;
  };
}
const arr = harvest(
  filter(integer(0, 42), function divisibleByThree(value) {
    return value % 3 === 0;
  }),
);

function concat(...generators) {
  const next = element(generators);
  let generator = next();

  return function concatGenerator(...args) {
    if (generator !== undefined) {
      const value = generator(...args);
      if (value === undefined) {
        generator = next();
        return concatGenerator(...args);
      }
      return value;
    }
  };
}

// the join factory takes a function and one or more generators and returns
// a new generator
// each time the new generator is called, it calls all of the old generators
// and pass their results to the function
function join(func, ...gens) {
  return function joinGenerator() {
    return func(...gens.map((gen) => gen()));
  };
}
function map(array, func) {
  return harvest(join(func, element(array)));
}
// console.log(
//   map([12, 3, -1, 199, 6], (x) => {
//     if (x === undefined) return x;
//     return x + 1;
//   }),
// );

function objectify(...names) {
  return function objectifyConstructor(...values) {
    const object = Object.create(null);
    names.forEach((name, index) => {
      object[name] = values[index];
    });
    return object;
  };
}
const obj = objectify("key1", "key2", "key3");
// console.log(obj("val1", "val2", "val3"));
