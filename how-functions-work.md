# How Functions Work

- the `function` operator makes function objects
- the `...` operator is allowed in argument lists and parameter lists
  - in argument lists, it's called **spread**
  - in parameter lists it's called **rest**
    - the rest parameter must be the last parameter in the parameter list

```javascript
function curry(func, ...zeroth) {
  return function (...first) {
    return func(...zeroth, ...first);
  };
}
```

- When a function is called, an **activation object** is created
  - which is a _hidden_ data structure that holds the information and bindings
    and the return address of the activation of the calling function
- JavaScript allocates activation objects on a **heap**, like ordinary objects
- activation objects are not automatically deactivated when functions return
  - they survive as long as there is a reference to it
  - they are garbage collected like ordinary objects
- The activation object contains:

  - a reference to the function object
  - a reference to the activation object of the _caller_
    - used by `return` to give control back
  - resumption info that is used to continue execution after a call
    - usually the address of an instruction that is immediately after
      a function call
  - parameters of the function, initialized with arguments
  - variables of the function, initialized with `undefined`
  - temp variables
  - `this`

- a function object has a `prototype` property
  - it contains a reference to an object that contains
    - a `constructor` property that contains a reference back to the function
      object, and
    - a delegation link to `Object.prototype`
- function object has a delegation link to `Function.prototype`
  - as a result, a function object inherits methods `apply` and `call`
- function object also contains two hidden properties
  - a reference to the function's executable code
  - a reference to the activation object that was active at the time the
    function object was created
    - this is what makes closure possible
    - the function can use this hidden property to access the variables of the
      function that created it
- **free variable**: the variables that a function uses that were declared
  outside of the function
- **bound variable**: variables declared inside of the function including the
  parameters
- **closure**
  - the mechanism that a function object holds a reference to the activation
    object of the outer function
