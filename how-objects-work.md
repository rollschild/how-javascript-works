# How Objects Work

- a property of an object can be:
  - a name followed by a parameter list wrapped in `()` followed by a function
    body wrapped in `{}`
    - this is a contraction for a name followed by a colon followed by
      a function expression
    - allows omitting the `: function`

```javascript
const myObject = {
  myMethod() {
    return "something";
  },
};
```

-

## Inheritance

- `Object.create(prototype)` takes an existing object and returns a new one that **inherits** from the **prototype**
- if there's an attempt to access a missing property
  - before returning `undefined`, the system first goes to the prototype, then its prototype, and so on
  - the same named property, if any, is returned from the prototype chain
- the most popular use of prototype is as a place to store functions
  - object literals inherit from `Object.prototype`
  - arrays inherit from `Array.prototype`
  - strings from `String.prototype`
  - numbers from `Number.prototype`
  - functions from `Function.prototype`
- `hasOwnProperty`
  - if the object has a property named `hasOwnProperty`,
  - it will be called instead of the `Object.prototype.hasOwnProperty` method
- `Object.create(prototype)` uses less memory than
  `Object.assign(Object.create({}))`
- `Object.create(null)` makes an object that inherits nothing
- `Object.keys(object)` returns all names of the **own** properties, **NOT**
  the inherited ones
  - the strings in the array will be in the order of insertion
- `Object.freeze()` and `Object.isFrozen`
  - can also be used to freeze arrays
  - only shallow freeze
- `Object.freeze()` operates on values
- `const` operates on variables
- `WeakMap`
  > The WeakMap object is a collection of key/value pairs in which the keys are weakly referenced.
  > Native WeakMaps hold "weak" references to key objects.
  > Meaning that they are a target of garbage collection (GC)
  - native `WeakMap`s do not prevent garbage collection
    > Because the references are weak, WeakMap keys are not enumerable

```javascript
function sealerFactory() {
  const weakmap = new WeakMap();
  return {
    sealer(object) {
      const box = Object.freeze(Object.create(null));
      weakmap.set(box, object);
      return box;
    },
    unsealer(box) {
      return weakmap.get(box);
    },
  };
}
```

## Weak references

- a strong reference is a reference that keeps an object in memory

```javascript
let dog = { name: "badger" };
const pets = [dog];
dog = null;
console.log(pets); // [{ name: "badger" }]
```

- weak reference doesn't prevent garbage collection

```javascript
let pets = new WeakMap();
let dog = { name: "badger" };

pets.set(dog, "okay");
console.log(pets); // WeakMap{ {...} -> "Okay" } <= dog set to the WeakMap

dog = null; // Overwrite the reference to the object
console.log(pets); // WeakMap(0) <= dog has been garbage collected.
```

-
