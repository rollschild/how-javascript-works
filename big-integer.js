const radix = 2 ** 24;
const radixSquared = radix * radix;
const log2Radix = 24;
const plus = "+";
const minus = "-";
const signIndex = 0;
const leastIndex = 1;

const lastElement = array => {
  return array.length > 0 ? array[array.length - 1] : undefined;
};
const nextToLastElement = array => {
  return array.length > 1 ? array[array.length - 2] : undefined;
};

const zero = Object.freeze([plus]);
const one = Object.freeze([plus, 1]);
const two = Object.freeze([plus, 2]);
const ten = Object.freeze([plus, 10]);
const negativeOne = Object.freeze([minus, 1]);

const isBigInteger = array => {
  return (
    Array.isArray(array) &&
    (array[signIndex] === plus || array[signIndex] === minus)
  );
};

const isNegativeBigInteger = array => {
  return Array.isArray(array) && array[signIndex] === minus;
};
const isPositiveBigInteger = array => {
  return Array.isArray(array) && array[signIndex] === plus;
};
const isZero = array => {
  return !Array.isArray(array) || array.length < 2;
};

const mint = protoBigInteger => {
  while (lastElement(protoBigInteger) === 0) {
    protoBigInteger.length -= 1;
  }

  if (protoBigInteger.length <= 1) return zero;

  if (protoBigInteger[signIndex] === plus) {
    if (protoBigInteger.length === 2) {
      // common words
      if (protoBigInteger[leastIndex] === 1) return one;
      if (protoBigInteger[leastIndex] === 2) return two;
      if (protoBigInteger[leastIndex] === 10) return ten;
    }
  } else {
    if (protoBigInteger.length === 2 && protoBigInteger[leastIndex] === 1) {
      return negativeOne;
    }
  }

  return Object.freeze(protoBigInteger);
};

const negate = big => {
  if (isZero(big)) return zero;
  const negation = big.slice();
  negation[signIndex] = isPositiveBigInteger(big) ? minus : plus;
  return mint(negation);
};

const abs = big => {
  return isZero(big) ? zero : isNegativeBigInteger(big) ? negate(big) : big;
};

const signum = big => {
  return isZero(big) ? zero : isNegativeBigInteger(big) ? negativeOne : one;
};

const eq = (comparahend, comparator) => {
  return (
    comparahend === comparator ||
    (comparahend.length === comparator.length &&
      comparahend.every((big, index) => big === comparator[index]))
  );
};

const absLessThan = (comparahend, comparator) => {
  return comparahend.length === comparator.length
    ? comparahend.reduce((reduction, element, index) => {
        if (index !== signIndex) {
          const otherElement = comparator[index];
          if (otherElement !== element) {
            return element < otherElement;
          }
        }

        return reduction;
      }, false)
    : comparahend.length < comparator.length;
};

const lessThan = (comparahend, comparator) => {
  return comparahend[signIndex] !== comparator
    ? isNegativeBigInteger(comparahend)
    : isNegativeBigInteger(comparahend)
    ? absLessThan(comparator, comparahend)
    : absLessThan(comparahend, comparator);
};

const greaterThanOrEqualTo = (a, b) => {
  return !lessThan(a, b);
};

const greaterThan = (a, b) => {
  return lessThan(b, a);
};

const lessThanOrEqualTo = (a, b) => {
  return !greaterThan(a, b);
};

/* bitwise operations
 * and, or, and xor */
const and = (a, b) => {
  if (a.length > b.length) {
    [a, b] = [b, a];
  }

  return mint(
    a.map((element, index) =>
      index === signIndex ? plus : element & b[index],
    ),
  );
};

const or = (a, b) => {
  if (a.length < b.length) {
    [a, b] = [b, a];
  }

  return mint(
    a.map((element, index) =>
      index === signIndex ? plus : element | (b[index] || 0),
    ),
  );
};

const xor = (a, b) => {
  if (a.length < b.length) {
    [a, b] = [b, a];
  }

  return mint(
    a.map((element, index) =>
      index === signIndex ? plus : element ^ (b[index] || 0),
    ),
  );
};

const int = big => {
  let result;
  if (typeof big === "number") {
    if (Number.isSafeInteger(big)) return big;
  } else if (isBigInteger(big)) {
    if (big.length < 2) return 0;
    if (big.length === 2) {
      return isNegativeBigInteger(big) ? -big[leastIndex] : big[leastIndex];
    }
    if (big.length === 3) {
      result = big[leastIndex] + big[leastIndex + 1] * radix;
      return isNegativeBigInteger(big) ? -result : result;
    }
    if (big.length === 4) {
      result =
        big[leastIndex] +
        big[leastIndex + 1] * radix +
        big[leastIndex + 2] * radixSquared;
      if (Number.isSafeInteger(result)) {
        return isNegativeBigInteger(big) ? -result : result;
      }
    }
  }
};

// downsizing and upsizing
// if the shift count is a multiple of 24, it would be easy
// otherwise we need to realign all of the bits
const shiftDown = (big, places) => {
  if (isZero(big)) return zero;
  places = int(places);
  if (Number.isSafeInteger(places)) {
    if (places === 0) return abs(big);
    if (places < 0) return shiftUp(big, -places);

    const skip = Math.floor(places / log2Radix);
    places -= skip * log2Radix;
    if (skip + 1 >= big.length) return zero;
    big = skip > 0 ? mint(zero.concat(big.slice(skip + 1))) : big;

    if (places === 0) return big;

    return mint(
      big.map((element, index) => {
        if (index === signIndex) return plus;

        // realign, if needed
        return (
          (radix - 1) &
          ((element >> places) |
            ((big[index + 1] || 0) << (log2Radix - places)))
        );
      }),
    );
  }
};

const shiftUp = (big, places) => {
  if (isZero(big)) return zero;
  places = int(places);
  if (Number.isSafeInteger(places)) {
    if (places === 0) return abs(big);
    if (places < 0) return shiftDown(big, -places);

    const blanks = Math.floor(places / log2Radix);
    const result = new Array(blanks + 1).fill(0);
    result[signIndex] = plus;
    places -= blanks * log2Radix;

    if (places === 0) return mint(result.concat(big.slice(leastIndex)));

    const carry = big.reduce((accumulator, element, index) => {
      if (index === signIndex) return 0;
      result.push(((element << places) | accumulator) & (radix - 1));
      return element >> (log2Radix - places);
    }, 0);

    if (carry > 0) result.push(carry);

    return mint(result);
  }
};

/*
 * Make a big integer out of some specific number of 1 bits
 * @param numberOfBits: number
 * @return bigInteger
 */
const mask = numberOfBits => {
  numberOfBits = int(numberOfBits);
  if (numberOfBits !== undefined && numberOfBits > 0) {
    const numberOfWholeDigits = Math.floor(numberOfBits / log2Radix);
    const result = new Array(numberOfWholeDigits + 1).fill(radix - 1);
    result[signIndex] = plus;
    const leftOver = numberOfBits - numberOfWholeDigits * log2Radix;
    if (leftOver > 0) {
      result.push((1 << leftOver) - 1);
    }

    return mint(result);
  }
};

const not = (big, numberOfBits) => {
  return xor(big, mask(numberOfBits));
};

const random = (numberOfBits, randomFunc = Math.random) => {
  const ones = mask(numberOfBits);
  if (ones !== undefined) {
    return mint(
      ones.map((element, index) => {
        if (index === signIndex) return plus;
        const bits = randomFunc();
        return ((bits * radixSquared) ^ (bits * radix)) & element;
      }),
    );
  }
};

const add = (augend, addend) => {
  if (isZero(augend)) return addend;
  if (isZero(addend)) return augend;

  if (augend[signIndex] !== addend[signIndex]) {
    return subtract(augend, negate(addend));
  }

  if (augend.length < addend.length) {
    [augend, addend] = [addend, augend];
  }

  let carry = 0;
  const result = augend.map((element, index) => {
    if (index !== signIndex) {
      element += addend[index] || 0;
      if (element >= radix) {
        carry = 1;
        element -= radix;
      } else {
        carry = 0;
      }
    }
    return element;
  });

  if (carry > 0) result.push(carry);

  return mint(result);
};

const subtract = (minuend, subtrahend) => {
  if (isZero(subtrahend)) return minuend;
  if (isZero(minuend)) return negate(subtrahend);

  let minuendSign = minuend[signIndex];
  if (minuendSign !== subtrahend[signIndex]) {
    return add(minuend, negate(subtrahend));
  }

  if (absLessThan(minuend, subtrahend)) {
    [minuend, subtrahend] = [subtrahend, minuend];
    minuendSign = minuendSign === minus ? plus : minus;
  }

  let borrow = 0;
  return mint(
    minuend.map((element, index) => {
      if (index === signIndex) return minuendSign;
      let diff = element - borrow - (subtrahend[index] || 0);
      if (diff < 0) {
        diff += radix;
        borrow = 1;
      } else {
        borrow = 0;
      }
      return diff;
    }),
  );
};

const multiply = (multiplicand, multiplier) => {
  if (isZero(multiplicand) || isZero(multiplier)) return zero;

  // initialize result array
  const result = [
    multiplicand[signIndex] === multiplier[signIndex] ? plus : minus,
  ];
  multiplicand.forEach((multiplicandElement, multiplicandIndex) => {
    if (multiplicandIndex !== signIndex) {
      let carry = 0;
      multiplier.forEach((multiplierElement, multiplierIndex) => {
        if (multiplierIndex !== signIndex) {
          const index = multiplierIndex + multiplicandIndex - 1;
          const product =
            multiplicandElement * multiplierElement +
            (result[index] || 0) +
            carry;

          result[index] = product & (radix - 1);
          carry = Math.floor(product / radix);
        }
      });

      if (carry > 0) {
        const productIndex = multiplicandIndex + multiplier.length - 1;
        result[productIndex] = (result[productIndex] || 0) + carry;
      }
    }
  });

  return mint(result);
};

/*
 * division
 * returns both quotient and remainder
 * @return [quotient, remainder]
 */
const divrem = (dividend, divisor) => {
  if (isZero(dividend) || absLessThan(dividend, divisor)) {
    return [zero, dividend];
  }
  if (isZero(divisor)) return undefined;

  // make the operands positive
  const isQuotientNegative = dividend[signIndex] !== divisor[signIndex];
  const isRemainderNegative = dividend[signIndex] === minus;
  let remainder = dividend;
  dividend = abs(dividend);
  divisor = abs(divisor);

  const shift = Math.clz32(lastElement(divisor)) - 8;
  dividend = shiftUp(dividend, shift);
  divisor = shiftUp(divisor, shift);
  let place = dividend.length - divisor.length;
  let dividendPrefix = lastElement(dividend);
  const divisorPrefix = lastElement(divisor);
  if (dividendPrefix < divisorPrefix) {
    dividendPrefix = dividendPrefix * radix + nextToLastElement(dividend);
  } else {
    place += 1;
  }
  divisor = shiftUp(divisor, (place - 1) * log2Radix);
  let quotient = new Array(place + 1).fill(0);
  quotient[signIndex] = plus;

  while (true) {
    let estimated = Math.floor(dividendPrefix / divisorPrefix);
    // the estimate will not be too small, but it might be too large
    if (estimated > 0) {
      while (true) {
        const trial = subtract(dividend, multiply(divisor, [plus, estimated]));
        if (!isNegativeBigInteger(trial)) {
          dividend = trial;
          break;
        }
        estimated -= 1;
      }
    }

    quotient[place] = estimated;
    place -= 1;
    if (place === 0) break;

    if (isZero(dividend)) break;
    dividendPrefix =
      lastElement(dividend) * radix + nextToLastElement(dividend);
    divisor = shiftDown(divisor, log2Radix);
  }

  // fix the remainder
  quotient = mint(quotient);
  remainder = shiftDown(dividend, shift);

  return [
    isQuotientNegative ? negate(quotient) : quotient,
    isRemainderNegative ? negate(remainder) : remainder,
  ];
};

const division = (dividend, divisor) => {
  const result = divrem(dividend, divisor);
  return result ? result[0] : undefined;
};

const power = (big, exponent) => {
  let exp = int(exponent);
  if (exp === 0) return one;
  if (isZero(big)) return zero;
  if (exp === undefined || exp < 0) return undefined;

  let result = one;
  while (true) {
    if ((exp & 1) !== 0) {
      result = multiply(result, big);
    }

    exp = Math.floor(exp / 2);
    if (exp < 1) break;

    big = multiply(big, big);
  }

  return mint(result);
};

const gcd = (a, b) => {
  a = abs(a);
  b = abs(b);

  while (!isZero(b)) {
    const [_, remainder] = divrem(a, b);
    a = b;
    b = remainder;
  }

  return a;
};

const digitset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ*~$=U";
const charset = (function (obj) {
  digitset.split("").forEach((element, index) => {
    obj[element] = index;
  });
  return Object.freeze(obj);
})(Object.create(null));

/*
 * Takes a number or a string and an optional radix, and
 * returns a big integer
 *
 * @param {number | string} value
 * @param {undefined | number} radix
 * @return {BigInteger}
 */
const make = (value, radixNumber) => {
  let result;
  if (typeof value === "string") {
    let radish;
    if (radixNumber === undefined) {
      radixNumber = 10;
      radish = ten;
    } else {
      if (
        !Number.isSafeInteger(radixNumber) ||
        radixNumber < 2 ||
        radixNumber > 37
      ) {
        return undefined;
      }
      radish = make(radixNumber);
    }

    result = zero;
    let good = false;
    let negative = false;
    if (
      value
        .toUpperCase()
        .split("")
        .every((element, index) => {
          const digit = charset[element];
          if (digit !== undefined && digit < radixNumber) {
            result = add(multiply(result, radish), [plus, digit]);
            good = true;
            return true;
          }
          if (index === signIndex) {
            if (element === plus) return true;
            if (element === minus) {
              negative = true;
              return true;
            }
          }
          return digit === "_"; // what does this mean?
        }) &&
      good
    ) {
      if (negative) {
        result = negate(result);
      }
      return mint(result);
    }
    return undefined;
  }

  if (Number.isInteger(value)) {
    let whole = Math.abs(value);
    result = [value < 0 ? minus : plus];

    while (whole >= radix) {
      const quotient = Math.floor(whole / radix);
      result.push(whole - quotient * radix);
      whole = quotient;
    }
    if (whole > 0) {
      result.push(whole);
    }
    return mint(result);
  }

  if (Array.isArray(value)) {
    return mint(value);
  }
};

/*
 * Converts a BigInteger to a JavaScript number
 * @param {BigInteger} big
 * @return {number} number
 */
const number = big => {
  let value = 0;
  let sign = 1;
  let factor = 1;
  big.forEach((element, index) => {
    if (index === signIndex) {
      if (element === minus) {
        sign = -1;
      }
    } else {
      value += element * factor;
      factor *= radix;
    }
  });
  return sign * value;
};

/*
 * Converts a BigInteger to string
 */
const string = (big, radixNumber = 10) => {
  if (isZero(big)) return "0";
  radixNumber = int(radixNumber);
  if (
    !Number.isSafeInteger(radixNumber) ||
    radixNumber < 2 ||
    radixNumber > 37
  ) {
    return undefined;
  }
  const radish = make(radixNumber);
  const sign = big[signIndex] === minus ? "-" : "";

  big = abs(big);
  const digits = [];
  while (!isZero(big)) {
    const [quotient, remainder] = divrem(big, radish);
    digits.push(digitset[number(remainder)]);
    big = quotient;
  }
  digits.push(sign);
  return digits.reverse().join("");
};

/*
 * Total count of 1 bits in a 32 bit integer
 * Can be used to compute the Hamming distance
 */
const population32 = int32 => {
  int32 -= (int32 >>> 1) & 0x55555555;
  int32 = (int32 & 0x33333333) + ((int32 >>> 2) & 0x33333333);
  int32 = (int32 + (int32 >>> 4)) & 0x0f0f0f0f;
  int32 = (int32 + (int32 >>> 8)) & 0x001f001f;
  return (int32 + (int32 >>> 16)) & 0x0000003f;
};
const population32Naive = int32 => {
  let count = 0;
  while (int32 > 0) {
    int32 = int32 & (int32 - 1);
    count++;
  }
  return count;
};

const population = big => {
  return big.reduce(
    (reduction, element, index) =>
      reduction + index === signIndex ? 0 : population32Naive(element),
    0,
  );
};

/*
 * Count the total number of bits excluding leading zeros
 */
const numberOfSignificantBits = big => {
  return big.length > 1
    ? make((big.length - 2) * log2Radix + (32 - Math.clz32(lastElement(big))))
    : zero;
};

export default Object.freeze({
  random,
  abs,
  absLessThan,
  add,
  and,
  division,
  divrem,
  eq,
  gcd,
  isBigInteger,
  isNegativeBigInteger,
  isPositiveBigInteger,
  isZero,
  lessThan,
  make,
  mask,
  multiply,
  negate,
  not,
  number,
  or,
  population,
  power,
  shiftDown,
  shiftUp,
  numberOfSignificantBits,
  signum,
  string,
  subtract,
  ten,
  two,
  one,
  xor,
  zero,
});
