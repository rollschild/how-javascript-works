[How Tail Calls Work](#how-tail-calls-work)

# How Tail Calls Work

- An optimization is a breaking of the rules, but breaking them in such a way
  that the breakage is not observable
- **The Tail Call Optimization**, a.k.a. **Proper Tail Calls**
  - eliminates a class of bugs
  - enables new paradigms of programming
  - any other ways of implementing tail calls is _improper_
- A tail call happens when the last thing a function does is return the result
  of calling a function

```javascript
function continuize(any) {
  return function hero(continuation, ...args) {
    return continuation(any(...args)); // <-- a tail call
  };
}
```

- the `return` instruction basically pops the call stack
- the optimization does this:

```assembly
jump continuation # go to the continuation function
```

- instead of this:

```assembly
call continuation
return
```

- Now we do not push the address of the return instruction onto the call stack
  - the `continuation` function returns to the function that called `hero`, not
    `hero` itself
- A tail call is like `goto` with arguments, but without any of the dangers of
  the `goto`
- How function calls work in JavaScript
  - Evaluate the argument expressions
  - Create an activation object of sufficient size to hold all of the
    function's parameters and variables
  - Store a reference to the function object that is being called in the new
    activation object
  - Store the values from the arguments into parameters in the new activation
    object; missing arguments are `undefined`; excess arguments are ignored
  - Store `undefined` into all variables in the new activation object
  - Set the `next instruction` field in the activation to indicate the
    instruction immediately after the call
  - Set the `caller` field in the new activation object to the current
    activation; not actually a **call stack**; it's actually a **linked
    list** of activations
  - Make the new activation the current activation
  - Begin execution of the called function
- How the optimization does it:
  - Evaluate the argument expressions
  - If the current activation is large enough,
    - use the current activation object as the new activation object
  - Otherwise,
    - Create an activation object _of sufficient size_ to hold all of the
      function's parameters and variables
    - Copy the `caller` field from the current activation object to the new
      activation object
    - Make the new activation object the current activation
  - Store a reference to the function object that is being called in the new
    activation object
  - Store the values from the arguments into parameters in the new activation
    object; missing arguments are `undefined`; excess arguments are ignored
  - Store `undefined` into all of the variables in the new activation object
  - Begin execution of the called function
- Differences:
  - If the current activation object is big enough, _which usually is_, we do
    not need to allocate another
  - call stack chains are not long, usually on the order of a few hundred
  - always allocate objects in a single maxy size when tail calls are imminent
  - With tail call optimization, recursive functions can become as fast as
    loops
    - loops are inherently **impure**
    - purity comes with recursion

```javascript
// loop
while (true) {
  // do something
  if (done) {
    break;
  }
  // do more things
}

// recursion
(function loop() {
  // do something
  if (done) {
    return;
  }
  // do more things
  return loop();
})();
```

- A call is a tail call if the value the function returns is returned

```javascript
return 1 + any(); // <-- NOT a tail call

any(); // <-- NOT a tail call
return;
```

- Recursion:

```javascript
function factorial(n) {
  if (n < 2) {
    return 1;
  }
  return n * factorial(n - 1); // <-- not a tail call
}

// will be optimized
// the recursive calls will not generate activation objects
function factorial(n, result = 1) {
  if (n < 2) {
    return result;
  }
  return factorial(n - 1, n * result); // <-- tail call
}
```

- `return function () {};` is **NOT** a tail call
- `return (function () {}());` **is** a tail call
- A tail call inside a `try` block **cannot** be optimized
  - the optimization would save memory by not linking the activation object for
    the activation object for this invocation into the call stack
  - BUT `try` might send control back to this invocation's `catch`
    - thus we need the activation object
- **Continuation Passing Style**
  - functions take an additional `continuation` parameter, which is a function
    that receives the result
  - a `continuation` is a function that represents the continuation of the
    program
