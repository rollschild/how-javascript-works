import BigFloat from "./big-floating-point.js";

// start by making some sets
// we need sets of things that produce boolean values, and JavaScript reserved
// words
function makeSet(array, value = true) {
  const object = Object.create(null);
  array.forEach((element) => {
    object[element] = value;
  });
  return $NEO.stone(object);
}

const booleanOperator = makeSet([
  "array?",
  "boolean?",
  "function?",
  "integer?",
  "not",
  "number?",
  "record?",
  "stone?",
  "text?",
  "=",
  "!=",
  "<",
  ">",
  "<=",
  ">=",
  "/\\",
  "\\/",
]);
const reserved = makeSet([
  "arguments",
  "await",
  "break",
  "case",
  "catch",
  "class",
  "const",
  "continue",
  "debugger",
  "default",
  "delete",
  "do",
  "else",
  "enum",
  "eval",
  "export",
  "extends",
  "false",
  "finally",
  "for",
  "function",
  "if",
  "implements",
  "import",
  "in",
  "Infinity",
  "instanceof",
  "interface",
  "let",
  "NaN",
  "new",
  "null",
  "package",
  "private",
  "protected",
  "public",
  "return",
  "static",
  "super",
  "switch",
  "this",
  "throw",
  "true",
  "try",
  "typeof",
  "undefined",
  "var",
  "void",
  "while",
  "with",
  "yield",
]);

// the `primordial` object contains mappings of things from Neo primordial
// space to JS space
const primordial = $NEO.stone({
  abs: "$NEO.abs",
  array: "$NEO.array",
  "array?": "Array.isArray",
  "bit and": "$NEO.bitand",
  "bit mask": "$NEO.bitmask",
  "bit or": "$NEO.bitor",
  "bit shift down": "$NEO.bitdown",
  "bit shift up": "$NEO.bitup",
  "bit xor": "$NEO.bitxor",
  "boolean?": "$NEO.boolean_",
  char: "$NEO.char",
  code: "$NEO.code",
  false: "false",
  fraction: "$NEO.fraction",
  "function?": "$NEO.function_",
  integer: "$NEO.integer",
  "integer?": "$NEO.integer_",
  length: "$NEO.length",
  neg: "$NEO.neg",
  not: "$NEO.not",
  null: "undefined",
  number: "$NEO.make",
  "number?": "$NEO.isBigFloat",
  record: "$NEO.record",
  "record?": "$NEO.record_",
  stone: "$NEO.stone",
  "stone?": "Object.isFronzen",
  text: "$NEO.text",
  "text?": "$NEO.text_",
  true: "true",
});

// whitespace
let indentation;
function indent() {
  indentation += 4;
}
function outdent() {
  indentation -= 4;
}

// at the beginning of each line we emit a line break and padding
function begin() {
  return "\n" + " ".repeat(indentation);
}

let frontMatter;
let operatorTransform;
let statementTransform;
let unique;

const rxSpaceQuestion = /[\u0020?]/g;
function mangle(name) {
  // replace space or ? with _
  // prepend reserved words with $
  return reserved[name] ? "$" + name : name.replace(rxSpaceQuestion, "_");
}

const rxMinusPoint = /[\-.]/g;
function numgle(number) {
  // make big decimal literals look as natural as possible by making them into
  // constants
  // a constant name starts with `$`
  // `-` or `.` is replaced with `_`
  const text = BigFloat.string(number.number);
  const name = "$" + text.replace(rxMinusPoint, "_");

  if (unique[name] !== true) {
    unique[name] = true;
    frontMatter.push("const " + name + ' = $NEO.number("' + text + '");\n');
  }

  return name;
}

// takes an operator token
// gets the operator's transform
function op(thing) {
  const transform = operatorTransform[thing.id];
  return typeof transform === "string"
    ? thing.zeroth === undefined
      ? transform
      : transform +
        "(" +
        expression(thing.zeroth) +
        (thing.oneth === undefined ? "" : ", " + expression(thing.oneth)) +
        ")"
    : transform(thing);
}

// the `expression` function handles general expression tokens
function expression(thing) {
  if (thing.id === "(number)") {
    return numgle(thing);
  }
  if (thing.id === "(text)") {
    return JSON.stringify(thing.text);
  }
  if (thing.alphameric) {
    return thing.origin === undefined ? primordial[thing.id] : mangle(thing.id);
  }
  return op(thing);
}

function arrayLiteral(array) {
  return (
    "[" +
    array
      .map((element) =>
        Array.isArray(element) ? arrayLiteral(element) : expression(element),
      )
      .join(", ") +
    "]"
  );
}

// records
// the fields are made by assignment
// wrap the assignment in immediately invoked function expression
/* (function (o) {
 *   $NEO.set(o, fooBear, $12_3);
 *   o["two part"] = two_part;
 * })(Object.create(null)); */
// in record literals, variable names are mangled, but field names are not
function recordLiteral(array) {
  indent();
  const padding = begin();
  const string =
    "(function (o) {" +
    array
      .map(
        (element) =>
          padding +
          (typeof element.zeroth === "string"
            ? "o[" +
              JSON.stringify(element.zeroth) +
              "] = " +
              expression(element.oneth) +
              ";"
            : "$NEO.set(o, " +
              expression(element.zeroth) +
              ", " +
              expression(element.oneth) +
              ");"),
      )
      .join("") +
    padding +
    "return o;";
  outdent();
  return string + begin() + "}(Object.create(null)))";
}

function assertBoolean(thing) {
  const string = expression(thing);
  return booleanOperator[thing.id] === true ||
    (thing.zeroth !== undefined &&
      thing.zeroth.origin === undefined &&
      booleanOperator[thing.zeroth.id])
    ? string
    : "$NEO.assertBoolean(" + string + ")";
}

// stringify arrays of statement tokens and wrap them in blocks
function statements(array) {
  const padding = begin();
  return array
    .map((statement) => padding + statementTransform[statement.id](statement))
    .join("");
}

function block(array) {
  indent();
  const string = statements(array);
  outdent();
  return "{" + string + begin() + "}";
}

// the `statementTransform` object contains the transform functions for all
// statements
statementTransform = $NEO.stone({
  break: (ignore) => {
    return "break;";
  },
  call: (thing) => {
    return expression(thing.zeroth) + ";";
  },
  def: (thing) => {
    return (
      "var " + expression(thing.zeroth) + " = " + expression(thing.oneth) + ";"
    );
  },
  export: (thing) => {
    const exportation = expression(thing.zeroth);
    return (
      "export default " +
      (exportation.startsWith("$NEO.stone(")
        ? exportation
        : "$NEO.stone(" + exportation + ")") +
      ";"
    );
  },
  fail: () => {
    return 'throw $NEO.fail("fail");';
  },
  if: function ifStatement(thing) {
    return (
      "if (" +
      assertBoolean(thing.zeroth) +
      ") " +
      block(thing.oneth) +
      (thing.twoth === undefined
        ? ""
        : " else " +
          (thing.twoth.id === "if"
            ? ifStatement(thing.twoth)
            : block(thing.twoth)))
    );
  },
  import: (thing) => {
    return (
      "import " +
      expression(thing.zeroth) +
      " from " +
      expression(thing.oneth) +
      ";"
    );
  },
  let: (thing) => {
    const right =
      thing.oneth.id === "[]"
        ? expression(thing.oneth.zeroth) + ".pop();"
        : expression(thing.oneth);
    if (thing.zeroth.id === "[]") {
      return expression(thing.zeroth.zeroth) + ".push(" + right + ");";
    }
    if (thing.zeroth.id === ".") {
      return (
        "$NEO.set(" +
        expression(thing.zeroth.zeroth) +
        ", " +
        JSON.stringify(thing.zeroth.oneth.id) +
        ", " +
        right +
        ");"
      );
    }
    if (thing.zeroth.id === "[") {
      return (
        "$NEO.set(" +
        expression(thing.zeroth.zeroth) +
        ", " +
        expression(thing.zeroth.oneth) +
        ", " +
        right +
        ");"
      );
    }
    return expression(thing.zeroth) + " = " + right + ";";
  },
  loop: (thing) => {
    return "while (true) " + block(thing.zeroth);
  },
  return: (thing) => {
    return "return " + expression(thing.zeroth) + ";";
  },
  var: (thing) => {
    return (
      "var " +
      expression(thing.zeroth) +
      (thing.oneth === undefined ? ";" : " = " + expression(thing.oneth) + ";")
    );
  },
});

// a _functino_ is a built in function that is accessed by putting a `f` prefix
// on an operator
const functino = $NEO.stone({
  "?": "$NEO.ternary",
  "|": "$NEO.default",
  "/\\": "$NEO.and",
  "//": "$NEO.or",
  "=": "$NEO.eq",
  "!=": "$NEO.ne",
  "<": "$NEO.lt",
  ">=": "$NEO.ge",
  ">": "$NEO.gt",
  "<=": "$NEO.le",
  "~": "$NEO.cat",
  "~~": "$NEO.cats",
  "+": "$NEO.add",
  "-": "$NEO.sub",
  ">>": "$NEO.max",
  "<<": "$NEO.min",
  "*": "$NEO.mul",
  "/": "$NEO.div",
  "[": "$NEO.get",
  "(": "$NEO.resolve",
});

// `operatorTransform` contains all operator transforms
operatorTransform = $NEO.stone({
  "?": (thing) => {
    indent();
    const padding = begin();
    const string =
      "(" +
      padding +
      assertBoolean(thing.zeroth) +
      padding +
      "? " +
      expression(thing.oneth) +
      padding +
      ": " +
      expression(thing.twoth);
    outdent();
    return string + begin() + ")";
  },
  "/\\": (thing) => {
    return (
      "(" +
      assertBoolean(thing.zeroth) +
      " && " +
      assertBoolean(thing.oneth) +
      ")"
    );
  },
  "\\/": (thing) => {
    return (
      "(" +
      assertBoolean(thing.zeroth) +
      " || " +
      assertBoolean(thing.oneth) +
      ")"
    );
  },
  "=": "$NEO.eq",
  "!=": "$NEO.ne",
  "<": "$NEO.lt",
  ">=": "$NEO.ge",
  ">": "$NEO.gt",
  "<=": "$NEO.le",
  "~": "$NEO.cat",
  "~~": "$NEO.cats",
  "+": "$NEO.add",
  "-": "$NEO.sub",
  ">>": "$NEO.max",
  "<<": "$NEO.min",
  "*": "$NEO.mul",
  "/": "$NEO.div",
  "|": (thing) => {
    return (
      "function (_0) {" +
      "return (_0 === undefined) ? " +
      +expression(thing.oneth) +
      " : _0);}(" +
      expression(thing.zeroth) +
      "))"
    );
  },
  "...": (thing) => {
    return "..." + expression(thing.zeroth);
  },
  ".": (thing) => {
    return (
      "$NEO.get(" + expression(thing.zeroth) + ', "' + thing.oneth.id + '")'
    );
  },
  "[": (thing) => {
    if (thing.oneth === undefined) {
      return arrayLiteral(thing.zeroth);
    }
    return (
      "$NEO.get(" +
      expression(thing.zeroth) +
      ", " +
      expression(thing.oneth) +
      ")"
    );
  },
  "{": (thing) => {
    return recordLiteral(thing.zeroth);
  },
  "(": (thing) => {
    return (
      expression(thing.zeroth) +
      "(" +
      thing.oneth.map(expression()).join(", ") +
      ")"
    );
  },
  "[]": "[]",
  "{}": "Object.create(null)",
  f: (thing) => {
    if (typeof thing.zeroth === "string") {
      return functino[thing.zeroth];
    }

    return (
      "$NEO.stone(function (" +
      thing.zeroth
        .map((param) => {
          if (param.id === "...") {
            return "..." + mangle(param.zeroth.id);
          }
          if (param.id === "|") {
            return mangle(param.zeroth.id) + " = " + expression(param.oneth);
          }
          return mangle(param.id);
        })
        .join(", ") +
      ") " +
      (Array.isArray(thing.oneth)
        ? block(thing.oneth)
        : "{return " + expression(thing.oneth) + ";}") +
      ")"
    );
  },
});

// exports a code generator function that takes a tree and returns JS source
// program
export default $NEO.stone(function codegen(tree) {
  frontMatter = ['import $NEO from "./neo.runtime.js"\n'];
  indentation = 0;
  unique = Object.create(null);
  const bulk = statements(tree.zeroth);
  return frontMatter.join("") + bulk;
});
