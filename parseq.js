// makes an error object
// for exceptions and cancellations
function makeReason(factoryName, excuse, evidence) {
  const reason = new Error(
    "parseq." + factoryName + (excuse === undefined ? "" : ": " + excuse),
  );
  reason.evidence = evidence; // non-standard
  return reason;
}

function getArrayLength(array, factoryName) {
  if (Array.isArray(array)) {
    return array.length;
  }

  if (array === undefined) {
    return 0;
  }

  throw makeReason(factoryName, "Not an array", array);
}

/* console.log((function(a, b = 1, c) {}).length);
 * // 1, only parameters before the first one with
 * // a default value is counted */
function checkCallback(callback, factoryName) {
  if (typeof callback !== "function" || callback.length !== 2) {
    throw makeReason(factoryName, "Not a callback function", callback);
  }
}

// make sure all elements in an array are requestor functions
function checkRequestors(requestorArray, factoryName) {
  if (
    requestorArray.some(
      (requestor) =>
        typeof requestor !== "function" ||
        requestor.length < 1 ||
        requestor.length > 2,
    )
  ) {
    throw makeReason(factoryName, "Bad requestors array", requestorArray);
  }
}

// the `run` function launches the requestors and manages timing, cancellation,
// and throttling
// action callback
// timeout callback
// timeLimit in milliseconds
// if all goes well, it calls all of the requestor functions in the array
// each of them might return a cancel function that is kept in the
// `cancelArray`
function run(
  factoryName,
  requestorArray,
  initialValue,
  action,
  timeout,
  timeLimit,
  throttle = 0,
) {
  let cancelArray = new Array(requestorArray.length);
  let nextNumber = 0;
  let timerId = 0;

  function cancel(reason = makeReason(factoryName, "Cancel.")) {
    if (timerId !== undefined) {
      clearTimeout(timerId);
      timerId = undefined;
    }

    // if anything is still going, cancel it
    if (cancelArray !== undefined) {
      cancelArray.forEach((cancel) => {
        try {
          if (typeof cancel === "function") {
            return cancel(reason);
          }
        } catch (ignore) {}
      });
      cancelArray = undefined;
    }
  }

  // `startRequestor` is not exactly recursive
  // it does not directly call itself
  // but it does return a function that might call itself
  function startRequestor(value) {
    if (cancelArray !== undefined && nextNumber < requestorArray.length) {
      // each requestor has a number
      let number = nextNumber;
      nextNumber += 1;

      const requestor = requestorArray[number];
      try {
        cancelArray[number] = requestor(function startRequestorCallback(
          value,
          reason,
        ) {
          // this callback is called by `requestor` when it's done
          // this callback function can only be called once
          if (cancelArray !== undefined && number !== undefined) {
            // we no longer need the cancel associated with this requestor
            cancelArray[number] = undefined;

            // call the `action` function to let the requestor know what
            // happened
            action(value, reason, number);

            // clear `number` so this callback cannot be used again
            number = undefined;

            // if there are any requestors that are still waiting to start,
            // then start the next one
            // if the next requestor is in a sequence, then it gets the most
            // recent `value`, the others get `initialValue`
            setTimeout(
              startRequestor,
              0,
              factoryName === "sequence" ? value : initialValue,
            );
          }
        },
        value);
      } catch (e) {
        action(undefined, e, number);
        number = undefined;
        startRequestor(value);
      }
    }
  }

  // if a timeout was requested, start the timer
  if (timeLimit !== undefined) {
    if (typeof timeLimit === "number" && timeLimit >= 0) {
      if (timeLimit > 0) {
        timerId = setTimeout(timeout, timeLimit);
      }
    } else {
      throw makeReason(factoryName, "Bad time limit", timeLimit);
    }
  }

  // `race` or `parallel` start all requestors at once
  // but `throttle` starts as many as it allows, and then as each requestor
  // finishes, another is started

  // `sequence` and `fallback` factories set `throttle` to 1
  if (!Number.isSafeInteger(throttle) || throttle < 0) {
    throw makeReason(factoryName, "Bad throttle", throttle);
  }
  let repeat = Math.min(throttle || Infinity, requestorArray.length);
  while (repeat > 0) {
    setTimeout(startRequestor, 0, initialValue);
    repeat -= 1;
  }

  return cancel;
}

// `parallel` can take a second array of requestors that get a more forgiving
// failure policy
// it returns a requestor that produces an array of values
function parallel(
  requiredArray,
  optionalArray,
  timeLimit,
  timeOption,
  throttle,
  factoryName = "parallel",
) {
  let requestorArray;
  let numberOfRequired = getArrayLength(requestorArray, factoryName);
  if (numberOfRequired === 0) {
    if (getArrayLength(optionalArray, factoryName) === 0) {
      requestorArray = [];
    } else {
      requestorArray = optionalArray;
      timeOption = true;
    }
  } else {
    if (getArrayLength(optionalArray, factoryName) === 0) {
      requestorArray = requiredArray;
      timeOption = undefined;
    } else {
      requestorArray = requiredArray.concat(optionalArray);
      if (timeOption !== undefined && typeof timeOption !== "boolean") {
        throw makeReason(factoryName, "Bad timeOption", timeOption);
      }
    }
  }

  checkRequestors(requestorArray, factoryName);
  return function parallelRequestor(callback, initialValue) {
    checkCallback(callback, factoryName);
    let numberOfPending = requestorArray.length;
    let numberOfPendingRequired = numberOfRequired;
    let results = [];
    if (numberOfPending === 0) {
      callback(factoryName === "sequence" ? initialValue : results);
      return;
    }

    let cancel = run(
      factoryName,
      requestorArray,
      initialValue,
      function parallelAction(value, reason, number) {
        // the `action` function gets the result of each requestor in the array
        // `parallel` wants to return an array of all the values it sees
        results[number] = value;
        numberOfPending -= 1;

        // if requestor was one of the requireds, make sure it was successful
        // if it failed, then the parallel operation fails
        if (number < numberOfRequired) {
          numberOfPendingRequired -= 1;
          if (value === undefined) {
            cancel(reason);
            callback(undefined, reason);
            callback = undefined;
            return;
          }
        }

        // if all have been processed or if the requireds have all succeeded
        // and there is no timeOption, then we are done
        if (
          numberOfPending < 1 ||
          (timeOption === undefined && numberOfPendingRequired < 1)
        ) {
          cancel(makeReason(factoryName, "Optional.", evidence));
          callback(factoryName === "sequence" ? results.pop() : results);
          callback = undefined;
        }
      },
      function parallelTimeout() {
        // when the timer fires, work stops unless we were under the `false`
        // time option
        // the `false` timer option puts no time limits on the requireds,
        // allowing the optionals to run until the requireds finish or the time
        // expires, whichever happens last
        const reason = makeReason(factoryName, "Timeout.", timeLimit);
        if (timeOption === false) {
          timeOption = undefined;
          if (numberOfPendingRequired < 1) {
            cancel(reason);
            callback(results);
          }
        } else {
          // time has expired
          // if all of the requireds were successful, then the parallel
          // operation is successful
          cancel(reason);
          if (numberOfPendingRequired < 1) {
            callback(results);
          } else {
            callback(undefined, reason);
          }
          callback = undefined;
        }
      },
      timeLimit,
      throttle,
    );
    return cancel;
  };
}

// `race` factory returns a requestor that starts all of the requestors in the
// array at once; the first success wins
function race(requestorArray, timeLimit, throttle) {
  const factoryName = throttle === 1 ? "fallback" : "race";

  if (getArrayLength(requestorArray, factoryName) === 0) {
    throw makeReason(factoryName, "No requestors.");
  }
  checkRequestors(requestorArray, factoryName);
  return function raceRequestor(callback, initialValue) {
    checkCallback(callback, factoryName);
    let numberOfPending = requestorArray.length;
    let cancel = run(
      factoryName,
      requestorArray,
      initialValue, // in parallel
      function raceAction(value, reason, number) {
        numberOfPending -= 1;
        if (value !== undefined) {
          // we have a winner
          cancel(makeReason(factoryName, "Loser.", number));
          callback(value);
          callback = undefined;
        } else if (numberOfPending < 1) {
          // no winner, signal a failure
          cancel(reason);
          callback(undefined, reason);
          callback = undefined;
        }
      },
      function raceTimeout() {
        let reason = makeReason(factoryName, "Timeout", timeLimit);
        cancel(reason);
        callback(undefined, reason);
        callback = undefined;
      },
      timeLimit,
      throttle,
    );

    return cancel;
  };
}

// a `fallback` is just a throttled race
function fallback(requestorArray, timeLimit) {
  return race(requestorArray, timeLimit, 1);
}

// a `sequence` is a throttled parallel with propagated values
function sequence(requestorArray, timeLimit) {
  return parallel(
    requestorArray,
    undefined,
    timeLimit,
    undefined,
    1,
    "sequence",
  );
}
