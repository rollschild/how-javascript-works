# How Names Work

- start all the names with a letter and end them with a letter
- _ordinal_ variable names: `thingNr`
- _cardinal_ variable names: `nrThings`
- all names in JS should start with a lowercase letter
- if a function invocationis prefixed with `new`, then it's called as a constructor
  - other wise called as a function
- Convention:
  - _all_ constructor functions should start with an uppercase letter
  - nothing else should ever start with an uppercase letter
- never use `new`???
