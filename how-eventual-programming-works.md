# How Eventual Programming Works

- A typical feature of sequential languages is blocking input and output
  - JavaScript does **NOT** implement this I/O model though
- Threads (whether real or virtual) are CPUs that all run at the same time whilst
  sharing the same memory
  - threads work well when running pure functions
- read-modify-write race
- Dangers of thread races can be mitigated by means of mutual exclusion, which
  can lock critical sections of memory, blocking the threads, preventing them
  from executing when there is contention
  - but locks can be computationally expensive
  - **deadlock**: there might be a blocked thread that is not able to release
    its locks

## Eventual Programming

- An **eventual function** is a function that returns immediately, possibly
  before the work requested is finished
  - the result will be communicated eventually in the future thru a _callback
    function_ or _message send_
- **Eventuality** provides a way to manage lots of activities without exposing
  applications to threads
- Eventual programming is built on two ideas:
  - callback functions
  - processing loop
- **callback function** - _will_ be called _in the future_ when something of
  interest happens, such as
  - a message has arrived
  - some work has finished
  - a human has communicated with the program
  - sensor has observed something
  - time has passed
  - something has gone wrong
- **processing loop**, a.k.a. **event loop** or **message loop**
  - it takes the highest priority event/message from the queue, and calls the
    callback function registered to receive the event/message
- The queue
  - holds incoming events/messages
- Events/messages more likely come from ancillary threads that are managing
  user inputs, the network, I/O, and interprocess communication
- The way to communicate with the main thread where the JS program runs is with
  the queue
- JavaScript uses a single thread
  - most of its vital state is in the closure of its functions, not on the
    stack

## Law of Turns

- **turn**: each iteration of the processing loop
- The Law of Turns:
  - **NEVER WAIT**
  - **NEVER BLOCK**
  - **FINISH FAST**
- it applies to callback functions that the processing loop calls, and to every
  function that they call, directly and indirectly
- a function is not allowed to loop while waiting for something to happen
- a function is not allowed to block the main thread
- in a browser, functions like `alert` are not allowed
- in Node.js, functions whose names contain `-Sync` suffix are not allowed
- a function that takes a long time to do its work is not allowed
- _Any_ violating function must either be corrected or isolated in a separate
  process
  - a process is like a thread that does not share its memory
  - a callback sends some work to a process; the process sends a message when
    the task is completed; the message goes into queue and eventually
    delivered

## Trouble in Serverland

- JavaScript was created for **event loops**
  - for **message loops** though, not so much
- Three mistakes
  - **Callback Hell**
  - **Promises**
  - **Async-Await**
- What the three mistakes have in common is the tight coupling of logic and
  control flow

## Requestors

- Each unit of work, which might be making a request of some server or db or
  launching a process, should be a separate function
- these functions take a callback function as their first argument
  - when the unit of work is completed, the result is passed to the callback
- **requestor**: a function that takes a callback and performs a unit of work

```javascript
function myRequestor(callback, value)
```

- a **callback** takes 2 arguments: `value` and `reason`

```javascript
function myCallback(value, reason)
```

- a requestor function may _optionally_ return a cancel function
  - can be called to cancel the work for any reason
  - it's **NOT** undo

```javascript
function myCancel(reason)
```

## Requestor Factories

- Much of the work is making factories that take arguments and return
  requestors

```javascript
function requestor(unary) {
  return function requestor(callback, value) {
    try {
      return callback(unary(value));
    } catch (exception) {
      return callback(undefined, exception);
    }
  };
}
```

- read a file in Node.js

```javascript
function readFile(dir, encoding = "utf-8") {
  return function readFileRequestor(callback, value) {
    return fs.readFile(dir + value, encoding, function (err, data) {
      return err ? callback(undefined, err) : callback(data);
    });
  };
}
```

- We can define factories that make requestors that communicate with other
  services in dialogs that may extend over many turns

```javascript
function factory(serviceAddress, arguments) {
  return function requestor(callback, value) {
    try {
      sendMessage(callback, serviceAddress, start, value, arguments);
    } catch (exception) {
      return callback(undefined, exception);
    }

    return function cancel(reason) {
      return sendMessage(undefined, serviceAddress, stop, reason);
    };
  };
}
```


## Exceptions

- An exception can unwind a stack, but nothing in a stack survives from turn to turn
- Exceptions do not have the power to communicate to a future turn that something failed in an earlier turn
  - nor can they travel back in time to the origin of the failing request
- A factory is allowed to throw an exception because the caller of the factory is still on the stack
- Requestors are never allowed to throw exceptions because there is nothing on the stack to catch them
- Requestors must not allow stray exceptions to escape
  - all exceptions must be caught and passed to the _callback_ function as a _reason_
