const getLen = (arr) => arr.length;

(function test() {
  const arr = [1, 2];
  let len = getLen(arr);
  console.log("len:", len);

  arr.push(23, 4);
  console.log("len:", len);
})();
