# How Arrays Work

- JavaScript's arrays are really objects
- In modern JS, arrays differ from objects in the following ways:
  - arrays have the `length` property
    - an array's `length` is **not** necessarily the number of elements in the
      array
    - it's instead the highest integer ordinal plus `1`
  - arrays inherit from `Array.prototype`
  - arrays are produced using array literals, `[]`
  - `JSON` treats arrays and objects very differently
- **ALWAYS** use `Array.isArray()` to determine if a value is an array
- `typeof someArray === "object"`
- `shift` and `unshift` can be much slower than `pop` and `push`, especially
  when the array is large
- `reduce` reduces an array to a single value
- `reverse()` reverses an array **IN PLACE**
- the `forEach()` method ignores the return value of the function it calls
- `forEach()` and `find()` are able to exit early
- `sort()` sorts the array **IN PLACE**
  - not able to sort a frozen array
  - not safe to sort a shared array
