function dump(strings, ...values) {
  return JSON.stringify(
    {
      strings,
      values,
    },
    undefined,
    8,
  );
}

const player1 = "Kobe Bryant";
const player2 = "Kawhi Leonard";
const player3 = "Jimmy Butler";

const result = dump`My favorite players are ${player1}, ${player2}, and ${player3}.`;
console.log(typeof result);
console.log(result);
