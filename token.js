import BigFloat from "./big-floating-point.js";

const rxUnicodeEscapement = /\\u\{([0-9A-F]{4,6})\}/g;

const rxCrLf = /\n|\r\n?/;

// matches a Neo token
/*
 * Capture groups
 *   1. whitespace
 *   2. comment
 *   3. alphameric
 *   4. number
 *   5. string
 *   6. punctuator
 */
const rxToken =
  /(\u0020+)|(#.*)|([a-zA-Z](?:\u0020[a-zA-Z]|[0-9a-zA-Z])*\??)|(-?\d+(?:\.\d+)?(?:e\-?\d+)?)|("(?:[^"\\]|\\(?:[nr"\\]|u\{[0-9A-F]{4,6}\}))*")|(\.(?:\.\.)?|\/\\?|\\\/?|>>?|<<?|\[\]?|\{\}?|[()}\].,:?!;~~~=!=<=>=&|+\-*%f$@\^_'`])/y;

// export a tokenizer factory
// this factory returns a generator that breaks the lines into token objects
// whitespace is not tokenized
export default Object.freeze(function tokenize(source, comment = false) {
  // takes a source and produces from it an array of token objects
  // if the `source` is not an array, then split it into lines
  // if `comment` is `true` then comments are included as token objects
  const lines = Array.isArray(source) ? source : source.split(rxCrLf);
  let lineNumber = 0;
  let line = lines[0];
  rxToken.lastIndex = 0;

  return function tokenGenerator() {
    if (line === undefined) {
      return;
    }
    let columnNumber = rxToken.lastIndex;
    if (columnNumber >= line.length) {
      rxToken.lastIndex = 0;
      lineNumber += 1;
      line = lines[lineNumber];
      return line === undefined ? undefined : tokenGenerator();
    }

    let captives = rxToken.exec(line);
    if (!captives) {
      return {
        id: "(error)",
        lineNumber,
        columnNumber,
        string: line.slice(columnNumber),
      };
    }

    // whitespace matched
    if (captives[1]) {
      return tokenGenerator();
    }

    // comment
    if (captives[2]) {
      return comment
        ? {
            id: "(comment)",
            comment: captives[2],
            lineNumber,
            columnNumber,
            columnTo: rxToken.lastIndex,
          }
        : tokenGenerator();
    }

    // name matched
    if (captives[3]) {
      return {
        id: captives[3],
        alphameric: true,
        lineNumber,
        columnNumber,
        columnTo: rxToken.lastIndex,
      };
    }

    // number literal
    if (captives[4]) {
      return {
        id: "(number)",
        readonly: true,
        number: BigFloat.normalize(BigFloat.make(captives[4])),
        text: captives[4],
        lineNumber,
        columnNumber,
        columnTo: rxToken.lastIndex,
      };
    }

    // text literal
    if (captives[5]) {
      // use `.replace` to convert `\u{xxxxxx}` to a codepoint and
      // `JSON.parse` to process the remaining escapes and remove the quotes
      return {
        id: "(text)",
        readonly: true,
        text: JSON.parse(
          captives[5].replace(rxUnicodeEscapement, function (_, code) {
            return String.fromCodePoint(parseInt(code, 16));
          }),
        ),
        lineNumber,
        columnNumber,
        columnTo: rxToken.lastIndex,
      };
    }

    // punctuator
    if (captives[6]) {
      return {
        id: captives[6],
        lineNumber,
        columnNumber,
        columnTo: rxToken.lastIndex,
      };
    }
  };
});
