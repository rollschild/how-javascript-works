import BigInteger from "./big-integer.js";
import { deconstruct } from "./deconstruct-number.js";

const isBigRational = (num) => {
  return (
    typeof num === "object" &&
    BigInteger.isBigInteger(num.numerator) &&
    BigInteger.isBigInteger(num.denominator)
  );
};

const isInteger = (num) => {
  return (
    BigInteger.eq(BigInteger.one, num.denominator) ||
    BigInteger.isZero(BigInteger.divrem(num.numerator, num.denominator)[1])
  );
};

// console.log(
//   isInteger({
//     numerator: ["+", 12],
//     denominator: ["+", 7],
//   }),
// );

const isNegatvie = (num) => {
  return BigInteger.isNegativeBigInteger(num.numerator);
};

const makeBigRational = (numerator, denominator) => {
  const newBigRational = Object.create(null);
  newBigRational.numerator = numerator;
  newBigRational.denominator = denominator;
  return Object.freeze(newBigRational);
};
const zero = makeBigRational(BigInteger.zero, BigInteger.one);
const one = makeBigRational(BigInteger.one, BigInteger.one);
const two = makeBigRational(BigInteger.two, BigInteger.one);

const negate = (num) => {
  return makeBigRational(BigInteger.negate(num.numerator), num.denominator);
};

const abs = (num) => {
  return isNegatvie(num) ? negate(num) : num;
};

const normalize = (num) => {
  const { numerator, denominator } = num;
  if (BigInteger.eq(numerator, denominator)) {
    return num;
  }
  const gcd = BigInteger.gcd(numerator, denominator);
  return gcd === BigInteger.one
    ? num
    : makeBigRational(
        BigInteger.division(numerator, gcd),
        BigInteger.division(denominator, gcd),
      );
};

const conformAddOrSubtract = (op) => {
  return (a, b) => {
    try {
      if (BigInteger.eq(a.denominator, b.denominator)) {
        return makeBigRational(op(a.numerator, b.numerator), a.denominator);
      }

      return normalize(
        makeBigRational(
          op(
            BigInteger.multiply(a.numerator, b.denominator),
            BigInteger.multiply(b.numerator, a.denominator),
          ),
          BigInteger.multiply(a.denominator, b.denominator),
        ),
      );
    } catch (ignore) {}
  };
};
const add = conformAddOrSubtract(BigInteger.add);
const subtract = conformAddOrSubtract(BigInteger.subtract);
// console.log(
//   add(
//     {
//       numerator: ["+", 12],
//       denominator: ["+", 7],
//     },
//     {
//       numerator: ["+", 12],
//       denominator: ["+", 5],
//     },
//   ),
// );

const inc = (num) => {
  return makeBigRational(
    BigInteger.add(num.numerator, num.denominator),
    num.denominator,
  );
};
const dec = (num) => {
  return makeBigRational(
    BigInteger.subtract(num.numerator, num.denominator),
    num.denominator,
  );
};

const multiply = (multiplicand, multiplier) => {
  return makeBigRational(
    BigInteger.multiply(multiplicand.numerator, multiplier.numerator),
    BigInteger.multiply(multiplicand.denominator, multiplier.denominator),
  );
};
const div = (a, b) => {
  return makeBigRational(
    BigInteger.multiply(a.numerator, b.denominator),
    BigInteger.multiply(a.denominator, b.numerator),
  );
};

const remainder = (a, b) => {
  const quotient = div(normalize(a), normalize(b));
  return makeBigRational(
    BigInteger.divrem(quotient.numerator, quotient.denominator),
  );
};

const reciprocal = (num) => {
  return makeBigRational(num.denominator, num.numerator);
};

const integer = (num) => {
  return num.denominator === BigInteger.one
    ? num
    : makeBigRational(
        BigInteger.division(num.numerator, num.denominator),
        BigInteger.one,
      );
};

const fraction = (num) => {
  return subtract(num, integer(num));
};

const eq = (comparahend, comparator) => {
  return comparahend === comparator
    ? true
    : BigInteger.eq(comparahend.denominator, comparator.denominator)
    ? BigInteger.eq(comparahend.numerator, comparator.numerator)
    : BigInteger.eq(
        BigInteger.multiply(comparahend.numerator, comparator.denominator),
        BigInteger.multiply(comparator.numerator, comparahend.denominator),
      );
};

const lessThan = (comparahend, comparator) => {
  return isNegatvie(comparahend) !== isNegatvie(comparator)
    ? isNegatvie(comparahend)
    : isNegatvie(subtract(comparahend, comparator));
};
// console.log(
//   lessThan(
//     {
//       numerator: ["-", 12],
//       denominator: ["+", 7],
//     },
//     {
//       numerator: ["+", 12],
//       denominator: ["+", 7],
//     },
//   ),
// );

/*
 * Takes a pair of numerator and denominator and makes an object;
 * the conversion is exact
 */
const numberPattern =
  /^(-?)(?:(\d+)(?:(?:\u0020(\d+))?\/(\d+)|(?:\.(\d*))?(?:e(-?\d+))?)|\.(\d+))$/;
/*
 * If two arguments, both will be converted to BigIntegers;
 * if the argument is a string, try to parse it as a mixed fraction or as
 * a decimal literal;
 * if the argument is a number, deconstruct it;
 * otherwise, assume that missing argument is 1
 */
const make = (numerator, denominator) => {
  if (denominator !== undefined) {
    numerator = BigInteger.make(numerator);
    if (numerator === BigInteger.zero) {
      return zero;
    }
    denominator = BigInteger.make(denominator);
    if (
      !BigInteger.isBigInteger(numerator) ||
      !BigInteger.isBigInteger(denominator) ||
      BigInteger.isZero(denominator)
    ) {
      return undefined;
    }

    if (BigInteger.isNegativeBigInteger(denominator)) {
      numerator = BigInteger.negate(numerator);
      denominator = BigInteger.abs(denominator);
    }
    return makeBigRational(numerator, denominator);
  }

  if (typeof numerator === "string") {
    console.log("type: string");
    const parts = numerator.match(numberPattern);
    if (!parts) {
      return undefined;
    }
    console.log("parts:", parts);
    // Groups:
    // 1. sign
    // 2. integer
    // 3. top
    // 4. bottom
    // 5. frac
    // 6. exp
    // 7. naked frac
    if (parts[7]) {
      // remember, this is division/rational
      return make(
        BigInteger.make(parts[1] + parts[7]),
        BigInteger.power(BigInteger.ten, parts[7].length),
      );
    }
    if (parts[4]) {
      const bottom = BigInteger.make(parts[4]);
      if (parts[3]) {
        return make(
          BigInteger.add(
            BigInteger.multiply(BigInteger.make(parts[1] + parts[2]), bottom),
            BigInteger.make(parts[3]),
          ),
          bottom,
        );
      }

      return make(parts[1] + parts[2], bottom);
    }

    const frac = parts[5] || "";
    const exp = (Number(parts[6]) || 0) - frac.length;
    if (exp < 0) {
      return make(
        parts[1] + parts[2] + frac,
        BigInteger.power(BigInteger.ten, -exp),
      );
    }
    return make(
      BigInteger.multiply(
        BigInteger.make(parts[1] + parts[2] + parts[5]),
        BigInteger.power(BigInteger.ten, exp),
      ),
      BigInteger.one,
    );
  }

  if (typeof numerator === "number" && !Number.isSafeInteger(numerator)) {
    // deconstruct the number first
    const { sign, coefficient, exponent } = deconstruct(numerator);
    if (sign < 0) {
      coefficient = -coefficient;
    }
    coefficient = BigInteger.make(coefficient);
    if (exponent >= 0) {
      return make(
        BigInteger.multiply(
          coefficient,
          BigInteger.power(BigInteger.two, exponent),
        ),
        BigInteger.one,
      );
    }
    return normalize(
      make(coefficient, BigInteger.power(BigInteger.two, -exponent)),
    );
  }

  return make(numerator, BigInteger.one);
};

/*
 * Converts a big rational into a JS number;
 * not guaratneed to be exact if the value is outside of safe integer
 */
const number = (num) => {
  return BigInteger.number(num.numerator) / BigInteger.number(num.denominator);
};

/*
 * converts a big rational to string;
 * exact
 */
const string = (num, nrPlaces) => {
  if (num === zero) {
    return "0";
  }
  const { numerator, denominator } = normalize(num);
  let [quotient, remainder] = BigInteger.divrem(numerator, denominator);
  let result = BigInteger.string(quotient);

  if (remainder !== BigInteger.zero) {
    remainder = BigInteger.abs(remainder);
    if (nrPlaces !== undefined) {
      const [fractus, residue] = BigInteger.divrem(
        BigInteger.multiply(
          remainder,
          BigInteger.power(BigInteger.ten, nrPlaces),
        ),
        denominator,
      );
      if (
        !BigInteger.absLessThan(
          BigInteger.multiply(residue, BigInteger.two),
          denominator,
        )
      ) {
        fractus = BigInteger.add(fractus, BigInteger.one);
      }
      result +=
        "." +
        BigInteger.string(fractus).padStart(BigInteger.number(nrPlaces), "0");
    } else {
      result =
        (result === "0" ? "" : result + " ") +
        BigInteger.string(remainder) +
        "/" +
        BigInteger.string(denominator);
    }
  }

  return result;
};
// console.log(
//   string(
//     {
//       numerator: ["+", 12],
//       denominator: ["+", 7],
//     },
//     ["+", 3],
//   ),
// );

export default Object.freeze({
  isBigRational,
  isInteger,
  isNegatvie,
  makeBigRational,
  zero,
  one,
  two,
  negate,
  abs,
  add,
  subtract,
  inc,
  dec,
  multiply,
  div,
  normalize,
  remainder,
  reciprocal,
  integer,
  fraction,
  eq,
  lessThan,
  make,
  number,
  string,
});
