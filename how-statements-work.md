# How Statements Work

- Two sorts of programming languages:
  - expression languages
  - statement languages

## Declaration

- `let` declares a new variable in the current scope
- `function` creates a function object and a variable to contain it
- the `function` declaration gets [hoisted](https://developer.mozilla.org/en-US/docs/Glossary/Hoisting)
  - the pieces are removed from where you placed the declaration and moved to
    the top of the function body or module
  - all `let` statements created by function declarations get hosted to the
    top, followed by all the assignments of function objects to those
    variables
- `const` variables require initialization and cannot be reassigned
  - greater purity?
- `Object.freeze()` works on values, not on variables
- `const` works on variables, not on values
- variables and values:
  - variables contain references to values
  - values never contain variables

## Expression

- JS allows three expression types in statements that actually make sense
  - assignments
  - invocations
  - `delete`
- an assignment statement replaces the reference in a variable, or modifies
  a mutable object/array
- an assignment statement contains 4 parts:
  1. **lvalue**
     - a variable, or
     - an expression that produces an object or array value, and a refinement
  2. assignment operator
     - `=`
     - `+=`
     - `-=`
     - `*=`
     - `/=`
     - `%=`
     - `**=`
     - `>>>=`
     - `>>=`
     - `<<=`
     - `&=`
     - `|=`
     - `^=`
  3. an expression
  4. semicolon
- we should **NOT** use `++` or `--`

## Branching

- Expression statements are **impure**
- usage of `switch` is **NOT** recommended
  - an object can be used as an alternative
  - the value matching the `case` variable is the key
  - also the hazard because of `this`
- `if` is strictly better
- provide booleans to `if`
  - not boolish values
- `else if` should not be used if the previous block ended in a disruption
- use ternary operator
  - wrap the entire ternary expression in parens
  - put a line break after the open paren and align the condition and the two
    consequences

```
let myTernary = (
  condition
  ? (
    anotherCondition
    ? "true"
    : "false"
  )
  :(
    yetAnotherCondition
    ? "also true"
    : "false again"
  )
);
```

## Looping

- the best way to write loops is to use [tail recursion](#how-tail-calls-work)

## Disruption

- statements should not need labels

## Potpourri

- `debugger` statement may cause a suspension of execution
