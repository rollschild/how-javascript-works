let theError;
function error(zeroth, oneth) {
  theError = {
    id: "(error)",
    zeroth,
    oneth,
  };
  throw "fail";
}

// the `primordial` object contains the object that are built into the language
// `readonly` disallows the `let` statement
const primordial = (function (ids) {
  const result = Object.create(null);
  ids.forEach((id) => {
    result[id] = Object.freeze({
      id,
      alphameric: true,
      readonly: true,
    });
  });
  return Object.freeze(result);
})([
  "abs",
  "array",
  "array?",
  "bit and",
  "bit mask",
  "bit or",
  "bit shift down",
  "bit shift up",
  "bit xor",
  "boolean?",
  "char",
  "code",
  "false",
  "fraction",
  "function?",
  "integer",
  "integer?",
  "length",
  "neg",
  "not",
  "number",
  "number?",
  "null",
  "record",
  "record?",
  "stone",
  "stone?",
  "text",
  "text?",
  "true",
]);

// three tokens will always be visible as we advance thru the stream
// the generator function supplies a stream of token objects
// `prevToken`, `token`, and `nextToken`
// `advance` uses the generator to cycle thru all token objects, skipping over
// the comments
let theTokenGenerator;
let prevToken;
let token;
let nextToken;

let nowFunction; // the function being processed currently
let loop; // array of loop exit status

const theEnd = Object.freeze({
  id: "(end)",
  precedence: 0,
  columnNumber: 0,
  columnTo: 0,
  lineNumber: 0,
});

function advance(id) {
  if (id !== undefined && id !== token.id) {
    return error(token, "expected '" + id + "'");
  }
  prevToken = token;
  token = nextToken;
  nextToken = theTokenGenerator() || theEnd;
}

function prelude() {
  // if `token` contains a space, split it, putting the first part in
  // `prevToken`
  if (token.alphameric) {
    let spaceAt = token.id.indexOf(" ");
    if (spaceAt > 0) {
      prevToken = {
        id: token.id.slice(0, spaceAt),
        alphameric: true,
        lineNumber: token.lineNumber,
        columnNumber: token.columnNumber,
        columnTo: token.columnNumber + spaceAt,
      };
      token.id = token.id.slice(spaceAt + 1);
      token.columnNumber = token.columnNumber + spaceAt + 1;
      return;
    }
  }

  return advance();
}

// manage whitespaces
let indentation;
function indent() {
  indentation += 4;
}

function outdent() {
  indentation -= 4;
}

function atIndentation() {
  if (token.columnNumber !== indentation) {
    return error(token, "expected at " + indentation);
  }
}

function isLineBreak() {
  return token.lineNumber !== prevToken.lineNumber;
}

function sameLine() {
  if (isLineBreak()) {
    return error(token, "unexpected linebreak");
  }
}

function lineCheck(open) {
  return open ? atIndentation() : sameLine();
}

// the `register` function declares a new variable in a function's scope
// the `lookup` function finds a variable in the most relevant scope
function register(theToken, readonly = false) {
  if (nowFunction.scope[theToken.id] !== undefined) {
    error(theToken, "already defined");
  }
  theToken.readonly = readonly;
  theToken.origin = nowFunction;
  nowFunction.scope[theToken.id] = theToken;
}

function lookup(id) {
  let definition = nowFunction.scope[id];

  // if failed, search the ancestor scopes
  if (definition === undefined) {
    let parent = nowFunction.parent;
    while (parent !== undefined) {
      definition = parent.scope[id];
      if (definition !== undefined) {
        break;
      }
      parent = parent.parent;
    }
    if (definition === undefined) {
      definition = primordial[id];
    }
    if (definition !== undefined) {
      nowFunction.scope[id] = definition;
    }
  }

  return definition;
}

// the `origin` property captures the function that created the variable
// the `scope` property holds all variables created/used in a function
// `parent` points to the function that created this function

// three objects contain the functions that are used to parse the features of
// the language: `statements`, `prefixes`, and `suffixes`

// these objects contain the functions that do the specialized parsing
const parseStatement = Object.freeze(null);
const parsePrefix = Object.freeze(null);
const parseSuffix = Object.freeze(null);

// `expression` function and its helper `argumentExpression` are the core
// an expression can be viewed as having two parts:
//   - a left part
//   - a right part
// left part is a literal/variable/prefix
// right part is suffix operator that might be followed by another expression
function argumentExpression(precedence = 0, open = false) {
  // top down operator precedence
  let definition;
  let left;
  let theToken = token;

  if (theToken.id === "(number)" || theToken.id === "(text)") {
    advance();
    left = theToken;
  } else if (theToken.alphameric === true) {
    definition = lookup(theToken.id);
    if (definition === undefined) {
      return error(theToken, "expected a variable");
    }
    left = definition;
    advance();
  } else {
    // token might be a prefix: `(`, `[`, `{`, `f`
    definition = parsePrefix[theToken.id];
    if (definition === undefined) {
      return error(theToken, "expected a variable");
    }
    advance();
    left = definition.parser(theToken);
  }
  // now we have the left part

  while (true) {
    theToken = token;
    definition = parseSuffix[theToken.id];
    if (
      token.columnNumber < indentation ||
      (!open && isLineBreak()) ||
      definition === undefined ||
      definition.precedence <= precedence
    ) {
      break;
    }
    lineCheck(open && isLineBreak());
    advance();
    theToken.class = "suffix";
    left = definition.parser(left, theToken);
  }

  return left;
}

// `precedence` determines how the suffix operator is parsed
// `class` property is "suffix", "statement", or undefined
function expression(precedence, open = false) {
  // does a whitespace check
  lineCheck(open);
  return argumentExpression(precedence, open);
}

function parseDot(left, theDot) {
  // the expression on the left must be a veriable or an expression that can
  // return an object (excluding object literals)
  if (
    !left.alphameric &&
    left.id !== "." &&
    (left.id !== "[" || left.oneth === undefined) &&
    left.id !== "("
  ) {
    return error(token, "expected a variable");
  }

  const theName = token;
  if (theName.alphameric !== true) {
    return error(theName, "expected a field name");
  }

  theDot.zeroth = left;
  theDot.oneth = theName;
  sameLine();
  advance();
  return theDot;
}

// the [] parser is passed the expression on the left and the [ token
// it verifies that the left expression is valid
// then calls `expression` to get the thing in the brackets
// if there's a line break after [ then it's an open experssion
// it advances past the ]
function parseSubscript(left, theBracket) {
  if (
    !left.alphameric &&
    left.id !== "." &&
    (left.id !== "[" || left.oneth === undefined) &&
    left.id !== "("
  ) {
    return error(token, "expected a variable");
  }

  theBracket.zeroth = left;
  if (isLineBreak()) {
    indent();
    theBracket.oneth = expression(0, true);
    outdent();
    atIndentation();
  } else {
    theBracket.oneth = expression();
    sameLine();
  }

  advance("]");
  return theBracket;
}

// the `ellipsis` parser is a special case
// it's only allowed in three places: parameter lists, argument lists, and
// array literals
function ellipsis(left) {
  if (token.id === "...") {
    const theEllipsis = token;
    sameLine();
    advance("...");
    theEllipsis.zeroth = left;
    return theEllipsis;
  }

  return left;
}

// the () parser parses function calls
// it calls `argumentExpression` for each argument
// an open form invocation lists the arguments vertically _without_ commas
function parseInvocation(left, theParen) {
  /*
   * function invocation:
   *     expression
   *     expression...
   */

  const args = [];
  if (token.id === ")") {
    sameLine();
  } else {
    const open = isLineBreak();
    if (open) {
      indent();
    }

    while (true) {
      lineCheck(open);
      args.push(ellipsis(argumentExpression()));
      if (token.id === ")" || token === theEnd) {
        break;
      }
      if (!open) {
        sameLine();
        advance(",");
      }
    }

    if (open) {
      outdent();
      atIndentation();
    } else {
      sameLine();
    }
  }

  advance(")");
  theParen.zeroth = left;
  theParen.oneth = args;
  return theParen;
}

// the `suffix` function builds the `parseSuffix` array
// it takes an operator and a precedence and an optional parser
// it can provide a default parser function that works for most of the
// operators
function suffix(
  id,
  precedence,
  optionalParser = function infix(left, theToken) {
    theToken.zeroth = left;
    theToken.oneth = expression(precedence);
    return theToken;
  },
) {
  // make an infix or suffix operator
  const theSymbol = Object.create(null);
  theSymbol.id = id;
  theSymbol.precedence = precedence;
  theSymbol.parser = optionalParser;
  parseSuffix[id] = Object.freeze(theSymbol);
}

suffix("|", 111, function parseDefault(left, theBar) {
  theBar.zeroth = left;
  theBar.oneth = expression(112);
  advance("|");
  return theBar;
});
suffix("?", 111, function thenElse(left, theThen) {
  theThen.zeroth = left;
  theThen.oneth = expression();
  advance("!");
  theThen.twoth = expression();
  return theThen;
});
suffix("/\\", 222);
suffix("\\/", 222);
suffix("~", 444);
suffix("~~", 444);
suffix("+", 555);
suffix("-", 555);
suffix("<<", 555);
suffix(">>", 555);
suffix("*", 666);
suffix("/", 666);
suffix(".", 777, parseDot);
suffix("[", 777, parseSubscript);
suffix("(", 777, parseInvocation);

// we treat relational operators a little differently to guard against
// a < b <= c errors
const relOp = Object.create(null);
function relational(operator) {
  relOp[operator] = true; // why?
  return suffix(operator, 333, function (left, theToken) {
    theToken.zeroth = left;
    theToken.oneth = expression(333);
    if (relOp[token.id] === true) {
      return error(token, "unexpected relational operator");
    }
    return theToken;
  });
}
relational("=");
relational("!=");
relational("<");
relational(">");
relational("<=");
relational(">=");

// the `prefix` function builds the `parsePrefix` array
// prefix operators do not need precedence
function prefix(id, parser) {
  const theSymbol = Object.create(null);
  theSymbol.id = id;
  theSymbol.parser = parser;
  parsePrefix[id] = Object.freeze(theSymbol);
}

prefix("(", function () {
  let result;
  if (isLineBreak()) {
    indent();
    result = expression(0, true);
    outdent();
    atIndentation();
  } else {
    result = expression(0);
    sameLine();
  }

  advance(")");
  return result;
});

// the array literal parser calls the `expression` function for each element
// Three ways to write an array literal:
//   - empty: []
//   - closed: [element1, element2, ...]
//   - open: `[
//
//     ]`
// semicolon ; makes two dimensional arrays
// [1, 2, 3; 4, 5, 6; 7, 8, 9]
prefix("[", function arrayLiteral(theBracket) {
  let matrix = [];
  let array = [];
  if (!isLineBreak()) {
    while (true) {
      array.push(ellipsis(expression()));
      if (token.id === ",") {
        sameLine();
        advance(",");
      } else if (token.id === ";" && array.length > 0 && nextToken !== "]") {
        sameLine();
        advance(";");
        matrix.push(array);
        array = [];
      } else {
        break;
      }
    }
    sameLine();
  } else {
    indent();
    while (true) {
      array.push(ellipsis(expression(0, isLineBreak())));
      if (token.id === "]" || token === theEnd) {
        break;
      }

      if (token.id === ";") {
        if (array.length === 0 || nextToken.id === "]") {
          break;
        }
        sameLine();
        advance(";");
        matrix.push(array);
        array = [];
      } else if (token.id === "," || !isLineBreak()) {
        sameLine();
        advance(",");
      }
    }
    outdent();
    if (token.columnNumber !== indentation) {
      return error(token, "expected at " + indentation);
    }
  }

  advance("]");
  if (matrix.length > 0) {
    matrix.push(array);
    theBracket.zeroth = matrix;
  } else {
    theBracket.zeroth = array;
  }

  return theBracket;
});

prefix("[]", function emptyArrayLiteral(theBrackets) {
  return theBrackets;
});

// record literal parser recognizes 4 forms of fields
//   - variable
//   - name: expression
//   - "string": expression
//   - [expression]: expression
prefix("{", function recordLiteral(theBrace) {
  const properties = [];
  let key;
  let value;
  const open = theBrace.lineNumber !== token.lineNumber;
  if (open) {
    indent();
  }

  while (true) {
    lineCheck(open);
    if (token.id === "[") {
      advance("[");
      key = expression();
      advance("]");
      sameLine();
      advance(":");
      value = expression();
    } else {
      key = token;
      advance();
      if (key.alphameric === true) {
        if (token.id === ":") {
          sameLine();
          advance(":");
          value = expression();
        } else {
          value = lookup(key.id);
          if (value === undefined) {
            return error(key, "expected a variable");
          }
        }
        key = key.id;
      } else if (key.id === "text") {
        key = key.text;
        sameLine();
        advance(":");
        value = expression();
      } else {
        return error(key, "expected a key");
      }
    }
    properties.push({
      zeroth: key,
      oneth: value,
    });

    if (token.columnNumber < indentation || token.id === "}") {
      break;
    }

    if (!open) {
      sameLine();
      advance(",");
    }
  }

  if (open) {
    outdent();
    atIndentation();
  } else {
    sameLine();
  }

  advance("}");
  theBrace.zeroth = properties;
  return theBrace;
});

prefix("{}", function emptyRecordLiteral(theBraces) {
  return theBraces;
});

// the function literal parser makes new functions
// it also gives access to _functinos_
const functino = (function makeSet(array, value = true) {
  const object = Object.create(null);
  array.forEach((element) => {
    object[element] = value;
  });
  return Object.freeze(object);
})([
  "?",
  "|",
  "/\\",
  "\\/",
  "=",
  "!=",
  "<",
  ">=",
  ">",
  "<=",
  "~",
  "~~",
  "+",
  "-",
  ">>",
  "<<",
  "*",
  "/",
  "[",
  "(",
]);

prefix("f", function functionLiteral(theFunction) {
  // if `f` is followed by a suffix operator, then produce the corresponding
  // **functino**
  const theOperator = token;
  if (
    functino[token.id] === true &&
    (theOperator.id !== "(" || nextToken.id === ")")
  ) {
    advance();
    if (theOperator.id === "(") {
      sameLine();
      advance(")");
    } else if (theOperator.id === "[") {
      sameLine();
      advance("]");
    } else if (theOperator.id === "?") {
      sameLine();
      advance("!");
    } else if (theOperator.id === "|") {
      sameLine();
      advance("|");
    }
    theFunction.zeroth = theOperator.id;
    return theFunction;
  }

  if (loop.length > 0) {
    return error(theFunction, "Do not make functions in loops");
  }

  theFunction.scope = Object.create(null);
  theFunction.parent = nowFunction;
  nowFunction = theFunction;

  // Function parameters come in 3 forms
  //   - name
  //   - name |default|
  //   - name...
  // the parameter list can be open or closed
  const parameters = [];
  if (token.alphameric === true) {
    let open = isLineBreak();
    if (open) {
      indent();
    }
    while (true) {
      lineCheck(open);
      let theParameter = token;
      register(theParameter);
      advance();
      if (token.id === "...") {
        parameters.push(ellipsis(theParameter));
        break;
      }
      if (token.id === "|") {
        advance("|");
        parameters.push(parseSuffix["|"](theParameter, prevToken));
      } else {
        parameters.push(theParameter);
      }
      if (open) {
        if (token.id === ",") {
          return error(token, "unexpected ','");
        }
        if (token.alphameric !== true) {
          true;
        }
      } else {
        if (token.id !== ",") {
          break;
        }
        sameLine();
        advance(",");
        if (token.alphameric !== true) {
          return error(token, "expected another parameter");
        }
      }
    }

    if (open) {
      outdent();
      atIndentation();
    } else {
      sameLine();
    }
  }
  theFunction.zeroth = parameters;

  // parse the `(return expression)` and `{function body}`
  if (token.id === "(") {
    advance("(");
    if (isLineBreak()) {
      indent();
      theFunction.oneth = expression(0, true);
      outdent();
      atIndentation();
    } else {
      theFunction.oneth = expression();
      sameLine();
    }
    advance(")");
  } else {
    // parse the function body
    // there must be an explicit `return`
    advance("{");
    indent();
    theFunction.oneth = statements();
    if (theFunction.oneth.return !== true) {
      return error(prevToken, "missing explicit 'return'");
    }

    // parse the `failure` handler
    if (token.id === "failure") {
      outdent();
      atIndentation();
      advance("failure");
      indent();
      theFunction.twoth = statements();
      if (theFunction.twoth.return !== true) {
        return error(prevToken, "missing explicit 'return'");
      }
    }
    outdent();
    atIndentation();
    advance("}");
  }

  nowFunction = theFunction.parent;
  return theFunction;
});

// the `statements` function parses statements, returning array of statement
// tokens
// it uses `prelude` to split the verb from the token, if necessary
function statements() {
  const statementList = [];
  let theStatement;
  while (true) {
    if (
      token === theEnd ||
      token.columnNumber < indentation ||
      token.alphameric !== true ||
      token.id.startsWith("export")
    ) {
      break;
    }
    atIndentation();
    prelude();
    let parser = parseStatement[prevToken.id];
    if (parser === undefined) {
      return error(prevToken, "expected a statement");
    }
    prevToken.class = "statement";
    theStatement = parser(prevToken);
    statementList.push(theStatement);

    // `disrupt` marks statements and statement lists that break or return
    // the `return` property marks statements and statement lists that return
    if (theStatement.disrupt === true) {
      if (token.columnNumber === indentation) {
        return error(token, "unreachable");
      }
      break;
    }
  }

  if (statementList.length === 0) {
    if (!token.id.startsWith("export")) {
      return error(token, "expected a statement");
    }
  } else {
    statementList.disrupt = theStatement.disrupt;
    statementList.return = theStatement.return;
  }

  return statementList;
}

// the `break` statement breaks out of the loop
parseStatement.break = function (theBreak) {
  if (loop.length === 0) {
    return error(theBreak, "'break' wants to be in a loop");
  }
  loop[loop.length - 1] = "break";
  theBreak.disrupt = true;
  return theBreak;
};

// `call` statement calls a function and ignores the return value
// for calls that are happening solely for their side effects
parseStatement.call = function (theCall) {
  theCall.zeroth = expression();
  if (theCall.zeroth.id !== "(") {
    return error(theCall, "expected a function invocation");
  }
  return theCall;
};

// `def` registers read only variables
parseStatement.def = function (theDef) {
  if (!token.alphameric) {
    return error(token, "expected a name.");
  }
  sameLine();
  theDef.zeroth = token;
  register(token, true);
  advance();
  sameLine();
  advance(":");
  theDef.oneth = expression();
  return theDef;
};

// try not to let exceptions corrupt into communication channels
parseStatement.fail = function (theFail) {
  theFail.disrupt = true;
  return theFail;
};

parseStatement.if = function ifStatement(theIf) {
  theIf.zeroth = expression();
  indent();
  theIf.oneth = statements();
  outdent();
  if (token.columnNumber === indentation) {
    if (token.id === "else") {
      advance("else");
      indent();
      theIf.twoth = statements();
      outdent();
      theIf.disrupt = theIf.oneth.disrupt && theIf.twoth.disrupt;
      theIf.return = theIf.oneth.return && theIf.twoth.return;
    } else if (token.id.startsWith("else if ")) {
      prelude();
      prelude();
      theIf.twoth = ifStatement(prevToken);
      theIf.disrupt = theIf.oneth.disrupt && theIf.twoth.disrupt;
      theIf.return = theIf.oneth.return && theIf.twoth.return;
    }
  }

  return theIf;
};

// `let` is the assignment statement
// the left side is more like an _lvalue_
parseStatement.let = function (theLet) {
  // the `let` statement is the only place where mutation is allowed
  sameLine();
  const name = token;
  advance();
  const id = name.id;
  let left = lookup(id);
  if (left === undefined) {
    return error(name, "expected a variable");
  }
  let readonly = left.readonly;

  // now we consider suffix operators [] . [ and {
  while (true) {
    if (token === theEnd) {
      break;
    }
    sameLine();

    // `[]` indicates an array append operation
    if (token.id === "[]") {
      readonly = false;
      token.zeroth = left;
      left = token;
      sameLine();
      advance("[]");
      break;
    }
    if (token.id === ".") {
      readonly = false;
      advance(".");
      left = parseDot(left, prevToken);
    } else if (token.id === "[") {
      readonly = false;
      advance("[");
      left = parseSubscript(left, prevToken);
    } else if (token.id === "(") {
      readonly = false;
      advance("(");
      left = parseInvocation(left, prevToken);
      if (token.id === ":") {
        return error(left, "assignment to the result of a function");
      }
    } else {
      break;
    }
  }

  advance(":");
  if (readonly) {
    return error(left, "assignment to a constant");
  }

  theLet.zeroth = left;
  theLet.oneth = expression();

  // `[]` at this position indicates an array pop operation
  if (
    token.id === "[]" &&
    left.id !== "[]" &&
    (theLet.oneth.alphameric === true ||
      theLet.oneth.id === "." ||
      theLet.oneth.id === "[" ||
      theLet.oneth.id === "(")
  ) {
    token.zeroth = theLet.oneth;
    theLet.oneth = token;
    sameLine();
    advance("[]");
  }

  return theLet;
};

// the `loop` statement keeps a stack for dealing with nested loops
// the entries in the stack are exit conditions of the loops
// if there is no exit, a stack's status is `"infinite"`
// if the only exit is a `return` statement, its status is `"return"`
// if the loops exits with `break`, then its status is `"break"`
// `fail` is not an explicit exit condition
parseStatement.loop = function (theLoop) {
  indent();
  loop.push("infinite");
  theLoop.zeroth = statements();
  const exit = loop.pop();
  if (exit === "infinite") {
    return error(theLoop, "A loop wants a 'break'.");
  }
  if (exit === "return") {
    theLoop.disrupt = true;
    theLoop.return = true;
  }

  outdent();
  return theLoop;
};

// the `return` statement changes the status of `"infinite"` loops to `"return"`
parseStatement.return = function (theReturn) {
  try {
    if (nowFunction.parent === undefined) {
      return error(theReturn, "'return' wants to be in a function.");
    }
    loop.forEach((element, elementNumber) => {
      if (element === "infinite") {
        loop[elementNumber] = "return";
      }
    });
    if (isLineBreak()) {
      return error(theReturn, "'return' wants a return value");
    }
    theReturn.zeroth = expression();
    if (token === "}") {
      return error(theReturn, "Misplaced 'return'.");
    }
    theReturn.disrupt = true;
    theReturn.return = true;
    return theReturn;
  } catch (ignore) {
    return theError;
  }
};

parseStatement.var = function (theVar) {
  if (!token.alphameric) {
    return error(token, "expected a name.");
  }
  sameLine();
  theVar.zeroth = token;
  register(token);
  advance();
  if (token.id === ":") {
    sameLine();
    advance(":");
    theVar.oneth = expression();
  }
  return theVar;
};

Object.freeze(parsePrefix);
Object.freeze(parseSuffix);
Object.freeze(parseStatement);

// `import` and `export` are not included in `parseStatement` because their
// placement in the source is restricted
function parseImport(theImport) {
  sameLine();
  register(token, true);
  theImport.zeroth = token;
  advance();
  sameLine();
  advance(":");
  sameLine();
  theImport.oneth = token;
  advance("(text)");
  theImport.class = "statement";
  return theImport;
}
function parseExport(theExport) {
  theExport.zeroth = expression();
  theExport.class == "statement";
  return theExport;
}

// export a single `parse` function
// it takes a token generator and returns a tree
export default function parse(tokenGenerator) {
  try {
    indentation = 0;
    loop = [];
    theTokenGenerator = tokenGenerator;
    nextToken = theEnd;
    const program = {
      id: "",
      scope: Object.create(null),
    };
    nowFunction = program;
    advance();
    advance();
    let theStatements = [];
    while (token.id.startsWith("import ")) {
      atIndentation();
      prelude();
      theStatements.push(parseImport(prevToken));
    }

    theStatements = theStatements.concat(statements());

    if (token.id.startsWith("export")) {
      atIndentation();
      prelude();
      theStatements.push(parseExport(prevToken));
    }

    if (token !== theEnd) {
      return error(token, "unexpected");
    }

    program.zeroth = theStatements;
    return program;
  } catch (ignore) {
    return theError;
  }
}
