# How Class Free Works

- **Inheritance** builds on the idea of **same as except**
- Inheritance causes the tight coupling of classes
- Good practice:
  - properties are hidden
  - methods process transactions and not just property mutation
- Factories: functions that return functions
- **Constructors**
  - functions that return objects containing functions

```javascript
function counterConstructor() {
  let counter = 0;

  function up() {
    counter += 1;
    return counter;
  }

  function down() {
    counter -= 1;
    return counter;
  }

  return Object.freeze({
    up,
    down,
  });
}
```

- the interface of the object is its methods and its methods only
- the methods should implement transactions
- Two types of objects:
  - the hard ones - containing only methods
    - they defend the integrity of the data held in closure
  - soft data objects
    - no behavior
- **Use a single object as the parameter of the constructor function**
- This approach to object construction uses more memory than the prototypal approach
  - every hard object contains all of the object's methods
  - whilst a prototypal object contains a reference to a prototype containing the methods
- Emphasis on transactions over properties
-
