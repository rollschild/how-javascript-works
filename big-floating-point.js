import BigInteger from "./big-integer.js";
import { deconstruct } from "./deconstruct-number.js";

/*
 * coefficient: big integers
 * basis: 10
 */

const isBigFloat = (big) => {
  return (
    typeof big === "object" &&
    BigInteger.isBigInteger(big.coefficient) &&
    Number.isSafeInteger(big.exponent)
  );
};

const isNegativeBigFloat = (big) => {
  return BigInteger.isNegativeBigInteger(big.coefficient);
};

const isPositiveBigFloat = (big) => {
  return BigInteger.isPositiveBigInteger(big.coefficient);
};

const isZero = (big) => {
  return BigInteger.isZero(big.coefficient);
};

const zero = (function () {
  const zeroObject = Object.create(null);
  zeroObject.coefficient = BigInteger.zero;
  zeroObject.exponent = 0;
  Object.freeze(zeroObject);
  return zeroObject;
})();

const makeBigFloat = (coefficient, exponent) => {
  if (BigInteger.isZero(coefficient)) return zero;

  const newBigFloat = Object.create(null);
  newBigFloat.coefficient = coefficient;
  newBigFloat.exponent = exponent;
  return Object.freeze(newBigFloat);
};

const bigIntegerTenMillion = BigInteger.make(10000000);

/*
 * Converts a big floating number into a JavaScript number
 */
const number = (num) => {
  const result = isBigFloat(num)
    ? num.exponent === 0
      ? BigInteger.isBigInteger(num.coefficient)
      : BigInteger.number(num.coefficient) * 10 ** num.coefficient
    : typeof num === "number"
    ? num
    : BigInteger.isBigInteger(num)
    ? BigInteger.number(num)
    : Number(num);

  return Number.isFinite(result) ? result : undefined;
};

const negate = (num) => {
  return makeBigFloat(BigInteger.negate(num.coefficient), num.exponent);
};

const abs = (num) => {
  return isNegativeBigFloat(num) ? negate(num) : num;
};

const conformAddOrSubtract = (op) => {
  return (a, b) => {
    const differential = a.exponent - b.exponent;
    return differential === 0
      ? makeBigFloat(op(a.coefficient, b.coefficient), a.exponent)
      : differential > 0
      ? makeBigFloat(
          op(
            BigInteger.multiply(
              a.coefficient,
              BigInteger.power(BigInteger.ten, differential),
            ),
            b.exponent,
          ),
        )
      : makeBigFloat(
          op(
            a.coefficient,
            BigInteger.multiply(
              b.coefficient,
              BigInteger.power(BigInteger.ten, -differential),
            ),
          ),
          a.exponent,
        );
  };
};

const add = conformAddOrSubtract(BigInteger.add);
const subtract = conformAddOrSubtract(BigInteger.subtract);

// console.log(
//   subtract(
//     { coefficient: ["+", 12], exponent: 2 },
//     { coefficient: ["+", 9], exponent: 10 },
//   ),
// );

const multiply = (multiplicand, multiplier) => {
  return makeBigFloat(
    BigInteger.multiply(multiplicand.coefficient, multiplier.coefficient),
    multiplicand.exponent + multiplier.exponent,
  );
};

const div = (dividend, divisor, precision = -4) => {
  if (isZero(dividend)) return 0;
  if (isZero(divisor)) return undefined;

  let { coefficient, exponent } = dividend;
  exponent -= divisor.exponent;
  if (typeof precision !== "number") {
    precision = number(precision);
  }

  // scale the coefficient to the desired precision
  if (exponent > precision) {
    coefficient = BigInteger.multiply(
      coefficient,
      BigInteger.power(BigInteger.ten, exponent - precision),
    );
    exponent = precision;
  }

  let remainder;
  [coefficient, remainder] = BigInteger.divrem(
    coefficient,
    divisor.coefficient,
  );

  // round the result if necessary
  if (
    !BigInteger.absLessThan(
      BigInteger.add(remainder, remainder),
      divisor.coefficient,
    )
  ) {
    coefficient = BigInteger.add(
      coefficient,
      BigInteger.signum(dividend.coefficient),
    );
  }

  return makeBigFloat(coefficient, exponent);
};
// console.log(
//   div(
//     { coefficient: ["+", 12], exponent: 3 },
//     { coefficient: ["+", 7], exponent: 6 },
//     -2,
//   ),
// );

/*
 * a big floating point number is normalized if the exponent is as close to
 * zero as possible without losing significance
 */
const normalize = (big) => {
  if (isZero(big)) return zero;
  let { coefficient, exponent } = big;
  if (coefficient.length < 2) return zero;
  if (exponent !== 0) {
    if (exponent > 0) {
      coefficient = BigInteger.multiply(
        coefficient,
        BigInteger.power(BigInteger.ten, exponent),
      );
      exponent = 0;
    } else {
      let quotient;
      let remainder;
      while (exponent <= -7 && (coefficient[1] & 127) === 0) {
        [quotient, remainder] = BigInteger.divrem(
          coefficient,
          bigIntegerTenMillion,
        );
        if (remainder !== BigInteger.zero) {
          break;
        }

        coefficient = quotient;
        exponent += 7;
      }

      while (exponent < 0 && (coefficient[1] & 1) === 0) {
        [quotient, remainder] = BigInteger.divrem(coefficient, BigInteger.ten);
        if (remainder !== BigInteger.zero) {
          break;
        }

        coefficient = quotient;
        exponent += 1;
      }
    }
  }

  return makeBigFloat(coefficient, exponent);
};

/*
 * converts a big integer, a string, or a JavaScript number to big floating
 * point
 */
/*
 * Capture groups:
 * [1] int
 * [2] fractionPart
 * [3] exp
 */
const numberPattern = /^(-?\d+)(?:\.(\d*))?(?:e(-?\d+))?$/;

const make = (a, b) => {
  // (BigInteger)
  if (BigInteger.isBigInteger(a)) {
    return makeBigFloat(a, b || 0);
  }

  if (typeof a === "string") {
    // (string, radix)
    if (Number.isSafeInteger(b)) {
      return make(BigInteger.make(a, b), 0);
    }

    // (string)
    let parts = a.match(numberPattern);
    if (parts) {
      let fractionPart = parts[2] || "";

      // notice it's the concantenation of strings, not addition of numbers
      return make(
        BigInteger.make(parts[1] + fractionPart),
        (Number(parts[3]) || 0) - fractionPart.length,
      );
    }
  }

  // (number, precision)
  if (typeof a === "number" && Number.isFinite(a)) {
    if (a === 0) return zero;

    let { sign, coefficient, exponent } = deconstruct(a);
    if (sign < 0) {
      coefficient = -coefficient;
    }

    coefficient = BigInteger.make(coefficient);

    if (exponent < 0) {
      return normalize(
        div(
          make(coefficient, 0),
          make(BigInteger.power(BigInteger.two, -exponent), 0),
          b,
        ),
      );
    }

    if (exponent > 0) {
      coefficient = BigInteger.multiply(
        coefficient,
        BigInteger.power(BigInteger.two, exponent),
      );
      exponent = 0;
    }
    return make(coefficient, exponent);
  }

  if (isBigFloat(a)) return a;
};
// console.log(make(20.37, -1));

const integer = (big) => {};

/*
 * Convert big float to string
 */
const string = (big, radix) => {
  if (isZero(big)) return "0";
  if (isBigFloat(radix)) {
    radix = normalize(radix);
    return radix && radix.exponent === 0
      ? BigInteger.string(big.coefficient, radix.coefficient)
      : undefined;
  }

  big = normalize(big);
  let s = BigInteger.string(BigInteger.abs(big.coefficient));
  console.log("s", s);
  if (big.exponent < 0) {
    // need to inset decimal point
    let point = s.length + big.exponent;
    if (point <= 0) {
      s = "0".repeat(1 - point) + s;
      point = 1;
    }
    s = s.slice(0, point) + "." + s.slice(point);
  } else if (big.exponent > 0) {
    s += "0".repeat(big.exponent);
  }

  if (BigInteger.isNegativeBigInteger(big.coefficient)) {
    s = "-" + s;
  }

  return s;
};
// console.log(string({ coefficient: ["-", 132], exponent: -2 }, 10));

/*
 * convert big floating point number into a string with the `e` notation
 */
const specific = (big) => {
  if (isZero(big)) return "0";
  big = normalize(big);
  let s = BigInteger.string(BigInteger.abs(big.coefficient));
  let e = big.exponent + s.length - 1;
  if (s.length > 1) {
    s = s.slice(0, 1) + "." + s.slice(1);
  }
  if (e !== 0) {
    s += "e" + e;
  }
  if (BigInteger.isNegativeBigInteger(big.coefficient)) {
    s = "-" + s;
  }

  return s;
};
// console.log(specific({ coefficient: ["-", 132], exponent: -4 }));

const lt = (a, b) => {
  return isNegativeBigFloat(subtract(a, b));
};

export default Object.freeze({
  abs,
  add,
  div,
  isBigFloat,
  isNegativeBigFloat,
  isPositiveBigFloat,
  isZero,
  lt,
  make,
  multiply,
  negate,
  normalize,
  number,
  specific,
  string,
  subtract,
  zero,
});
