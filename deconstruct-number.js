export function deconstruct(number) {
  /*
   * number = sign * coefficient * (2 ** exponent)
   */

  let sign = 1;
  let coefficient = number;
  let exponent = 0;

  if (coefficient < 0) {
    coefficient = -coefficient;
    sign = -1;
  }

  if (Number.isFinite(number) && number !== 0) {
    exponent = -1128;
    let reduction = coefficient;
    while (reduction !== 0) {
      reduction /= 2;
      exponent += 1;
    }

    /*
     * when the exponent is zero, the number can be viewed as an integer
     * if the exponent is not zero, then adjust to correct the coefficient
     */
    reduction = exponent;
    while (reduction > 0) {
      coefficient /= 2;
      reduction -= 1;
    }
    while (reduction < 0) {
      coefficient *= 2;
      reduction += 1;
    }
  }

  return {
    sign,
    coefficient,
    exponent,
    number,
  };
}

// console.log("1: ", deconstruct(1));
// console.log("MAX_SAFE_INTEGER: ", deconstruct(Number.MAX_SAFE_INTEGER));
