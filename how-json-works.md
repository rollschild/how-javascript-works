# How JSON works

- Standards should be simple and complete
  - the less we have agree to, the more easily we can interoperate
- JSON is about connecting programs written in different languages thru the
  network
- Unicode has a pair of invisible control characters, PS (paragraph separator)
  and LS (line separator)
  - JavaScript treats them as line ending characters, just like CR (carriage
    return) and LF (line feed)
- Java and JavaScript both use IEEE 754 binary floating point
- `Infinity` and `NaN` indicate errors
  - we should **NOT** put bad data on the wire
  - we should **NOT** propagate bad data
- JSON does **NOT** attach a meaning to `null`
  - that is for users of JSON to decide

## The JSON Object

- two functions in the `JSON` object
  - `parse`
  - `stringify`

```javascript
JSON.parse(text, reviver);
```

- the `reviver` function can make transformations
  - it's given a key and a value, and returns the desired value for that key

```javascript
const result = JSON.parse(text, function (key, value) {
  return typeof value === "string" &&
    (key.endsWith("Date") || rxIsoDate.match(value))
    ? new Date(value)
    : value;
});
```

- the `stringify` function takes a value and encodes it into a JSON text

```javascript
JSON.stringify(value, replacer, space);
```

- the optional replacer function can make transformations

```javascript
const jsonText = JSON.stringify(myObject, function (key, value) {
  return value instanceof Date ? value.toISOString() : value;
});
// Date.prototype.toJSON()
```

- the `replacer` argument can also be an array of strings
  - only properties with names in the array are included in the text

## Security Implications

- JSON text should **NEVER** be built by concatenation because a piece of
  malicious string could contain quote and backslash characters that could
  cause misinterpretation
- JSON text should **always** be made with encoders like `JSON.stringify` or
  similar tools
-
