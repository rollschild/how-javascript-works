# How Programs Work

- JS is distributed in source units
  - a source unit is a `.js` file
  - but can also be a source string from a JSON object, or a source text from
    a database or source management system
- Embedding JavaScript source units into an HTML page is a bad practice
  - bad for design because there is no separation between presentation and
    behavior
  - bad for performance because code in a page cannot be gzipped or cached
  - _very_ bad for security because it enables Cross Site Scripting (XSS)
    attacks
    - CSP (Content Security Policy) should be used consistently to prevent all
      use of inline source
- a source unit is treated like the body of a function with no parameters
- About inline event handlers:
  - event handler functions created by the source unit are not invoked
    immediately
  - instead it's bound to a DOM node and is invoked as events are fired

## Global

- back then all variable declarations outside of a function were added to the
  _page_ scope, a.k.a. the `global` scope
  - that scope also contained variables `window` and `self` that contained
    references to the page scope

## Module

- It's better to have all variable declarations outside of a function be added
  to the **module** scope
- a module should export one thing, typically a function or an object full of
  functions
- it's a good idea to freeze the exportation to prevent accidents or cross
  contamination
- Put your imports at the top of the file and exports at the bottom

## Cohesion and Coupling

- Good modules have strong cohesion and loose coupling
