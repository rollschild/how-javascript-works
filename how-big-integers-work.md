# How Big Integers Work

- Represent big integers in arrays
- How many bits per element?
  - 53 bits vs. 32 bits
  - 53 bits is the largest size of a positive safe integer
  - 32 bits or smaller allow for proper bitwise operations
  - But if we decided to use JavaScript's multiplication/division, which is
    only accurate to 53 bits, we need to determine a word size no more that
    half that
  - we can choose 24 bits
- the zeroth element of an array contains the sign of the number
  - `"+"` or `"-"`
- the first element contains the least significant word/digit
- base is `2 ** 24` = 1677216
- `Math.clz32()`: returns the number of leading zero bits in the 32-bit binary representation of a number
