# How WAT! Works

- **ALWAYS** use `===`
- **NEVER** use `==`

```javascript
[] == [] // false
[] == ![] // true
```

- what happens here is:
  - an empty array is truthy, so `![]` is `false`
  - `==` wants to compare `[]` and `false`
  - the empty array is coerced to empty string which is coerced to `0`
  - `false` is also coerced to `0`

```javascript
[] + [] // ""
[] + {} // "[object Object]"
{} + {} // "[object Object][object Object]"
```

- what happens here is:
  - because the values are not numbers, `+` wants to concatenate them
  - first it has to coerce them into strings
  - `Array.prototype.toString()` converts an empty array to empty string
  - `Object.prototype.toString()` renders the object as `"[object Object]"`
  - then they are concatenated together

```javascript
"2" + 1; // "21"
"2" - 1; // 1
```

- other arithmetic operators also do type coercion, but they use very different
  rules than `+` uses

```javascript
Math.min() > Math.max(); // true
```

- `Math.min()` returns `Infinity`
- `Math.max()` returns `-Infinity`
- Use `Number.isNaN` and `Number.isFinite`
  - do **NOT** use the global `isNaN` or `isFinite`

```javascript
((name) => [name])("wat"); // ["wat"]
((name) => {name})("wat"); // undefined
```

- 
