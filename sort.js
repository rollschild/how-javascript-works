const refine = (collection, path) => {
  return path.reduce((refinement, element) => {
    try {
      return refinement[element];
    } catch (ignore) {}
  }, collection);
};

export default function by(...keys) {
  const paths = keys.map((element) => element.toString().split("."));
  return (first, second) => {
    let firstValue;
    let secondValue;
    if (
      paths.every((path) => {
        firstValue = refine(first, path);
        secondValue = refine(second, path);
        return firstValue === secondValue;
      })
    ) {
      return 0;
    }

    return (
      typeof firstValue === typeof secondValue
        ? firstValue < secondValue
        : typeof firstValue < typeof secondValue
    )
      ? -1
      : 1;
  };
}

const people = [
  {
    first: "Frank",
    last: "Farkel",
  },
  {
    first: "Fanny",
    last: "Farkel",
  },
  {
    first: "Sparkel",
    last: "Farkel",
  },
  {
    first: "Charcoal",
    last: "Farkel",
  },
  {
    first: "Simon",
    last: "Farkel",
  },
  {
    first: "Ferd",
    last: "Berfel",
  },
];

// console.log(people.sort(by("last", "first")));
