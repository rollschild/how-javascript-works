const rxDeleteDefault = /[ < > & % " \\ ]/g;
// const rxSyntacticVariable = /
//   \{
//   (
//     [^ { } : \s ]+
//   )
//   (?:
//     :
//     (
//       [^ { } : \s ]+
//     )
//   )?
//   \}
// /g;
const rxSyntacticVariable = /\{([^ { } : \s ]+)(?::([^ { } : \s ]+))?\}/g;
function defaultEncoder(replacement) {
  return String(replacement).replace(rxDeleteDefault, "");
}

// Note that the function will be invoked multiple times for each full match to be replaced if the regular expression in
// the first parameter is global.
function fulfill(string, container, encoder = defaultEncoder) {
  return string.replace(rxSyntacticVariable, function (
    original,
    path,
    encoding = "",
  ) {
    console.log("original:", original);
    console.log("path:", path);
    console.log("encoding:", encoding);
    try {
      let replacement =
        typeof container === "function"
          ? container
          : path
              .split(".")
              .reduce((refinement, element) => refinement[element], container);
      console.log("replacement:", replacement);

      if (typeof replacement === "function") {
        replacement = replacement(path, encoding);
      }
      console.log("replacement:", replacement);

      replacement = (typeof encoder === "object" ? encoder[encoding] : encoder)(
        replacement,
        path,
        encoding,
      );
      console.log("replacement after encoding:", replacement);

      if (typeof replacement === "number" || typeof replacement === "boolean") {
        replacement = String(replacement);
      }
      console.log("replacement:", replacement);

      return typeof replacement === "string" ? replacement : original;
    } catch (ignore) {
      return original;
    }
  });
}

const example = fulfill(
  "{greeting}, {my.place:upper}! :{",
  {
    greeting: "Hello",
    my: {
      place: "World",
    },
  },
  {
    upper: function upper(string) {
      return string.toUpperCase();
    },
    "": function identity(string) {
      return string;
    },
  },
);
console.log("Example:", example);

export default Object.freeze(fulfill);
