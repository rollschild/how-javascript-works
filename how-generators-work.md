# How Generators Work

- Generators
  - produce a function object that produces an object that contains
    a `next` method that is made of the `function*` body
  - it has a `yield` operator that resembles a `return` statement
    - which produces an object that contains a `value` property that contains
      the expected value

## A better way to do generators

- start with a function that returns a function
  - a.k.a. a **factory**

```javascript
function factory(factoryParameters) {
  // initialization of generator's state

  return function generator(generatorParameters) {
    // update the state
    return value;
  };
}
```

- a **predicate** function is a function that returns `true` or `false`
