# How Exceptions Work

- JS allows you to `throw` any value
  - although it's common to `throw` something made by the `Error` constructor
- the `catch` clause receives whatever it was that `throw` threw
- every statement in a function could have its own `try` and its own `catch`
- **A function should not contain more than one `try`**

## Unwinding

- An important goal of exception management:
  - it does not impose a performance penalty on correctly running programs
- JS compiler produces a **catchmap** for every function that it compiles
- the catchmap maps instruction locations in the function body to the `catch`
  that handles those locations
- when a `throw` executed, raising an exception, the catchmap for the current
  function is consulted,
  - if there is a designated `catch` clause, then it gets control and execution
    resumes from there

## Ordinary Exceptions

- The most popular misuse of exceptions: to use them to communicate normal
  results
  - for example, when reading a file, _file not found_ should **NOT** be an
    exception
    - it's a normal occurrence
  - exceptions should **only** be used for problems that should not be
    anticipated
