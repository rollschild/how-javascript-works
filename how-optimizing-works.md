# How Optimizing Works

- We should only optimize where we can get a significant improvement
- If there is not a significant speed up, then we can call the change a bug
- Some biggest time sucks:
  - Failure to parallelize
  - Violating the law of turns
  - Weak cohesiveness
  - Tight coupling
  - Wrong algorithm
  - Thrashing
  - Bloat
  - Other people's code
