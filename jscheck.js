import fulfuill from "./fulfuill";

function resolve(value, ...rest) {
  return typeof value === "function" ? value(...rest) : value;
}

function literal(value) {
  return function () {
    return value;
  };
}

// produces a generator that makes booleans
function boolean(bias = 0.5) {
  bias = resolve(bias);
  return function () {
    return Math.random() < bias;
  };
}

function number(from = 1, to = 0) {
  from = Number(resolve(from));
  to = Number(resolve(to));
  if (from > to) {
    [from, to] = [to, from];
  }
  const difference = to - from;
  return function () {
    return Math.random() * difference + from;
  };
}

// `oneOf` takes an array of values and generators, and returns a generator
// that randomly returns those values
function oneOf(array, weights) {
  if (
    !Array.isArray(array) ||
    array.length < 1 ||
    (weights !== undefined &&
      (!Array.isArray(weights) || array.length !== weights.length))
  ) {
    throw new Error("JSCheck oneOf bad arguments.");
  }

  if (weights === undefined) {
    return function () {
      return resolve(array[Math.floor(Math.random() * array.length)]);
    };
  }

  const total = weights.reduce((prev, curr) => prev + curr, 0);
  let base = 0;
  const list = weights.map((value) => {
    base += value;
    return base / total;
  });
  return function () {
    let x = Math.random();
    return resolve(array[list.findIndex((element) => element >= x)]);
  };
}

function sequence(seq) {
  seq = resolve(seq);
  if (!Array.isArray(seq)) {
    throw new Error("JSCheck sequence.");
  }
  let elementIndex = -1;
  return function () {
    elementIndex += 1;
    if (elementIndex >= seq.length) {
      elementIndex = 0;
    }
    return resolve(seq[elementIndex]);
  };
}

const bottom = [false, null, undefined, "", 0, NaN];
function falsy() {
  return oneOf(bottom);
}

const primes = [
  2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
  73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151,
  157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233,
  239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317,
  331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419,
  421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503,
  509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607,
  613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701,
  709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811,
  821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911,
  919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997,
];

// `integer` returns a generator that returns integers in a selected range
// if range not specified, it returns a generator that returns random prime
// numbers under 1,000
function integerValue(value, defaultValue) {
  value = resolve(value);
  return typeof value === "number"
    ? Math.floor(value)
    : typeof value === "string"
    ? value.charCodeAt(0)
    : defaultValue;
}
function integer(i, j) {
  if (i === undefined) {
    return oneOf(primes);
  }
  i = integerValue(i, 1);
  if (j === undefined) {
    j = i;
    i = 1;
  } else {
    j = integerValue(j, 1);
  }
  if (i > j) {
    [i, j] = [j, i];
  }
  return function () {
    return Math.floor(Math.random() * (j - i + 1) + i);
  };
}

function character(i, j) {
  if (i === undefined) {
    return character(32, 126);
  }
  // https://stackoverflow.com/a/36527722
  if (typeof i === "string") {
    return j === undefined
      ? oneOf(i.split(""))
      : character(i.codePointAt(0), j.codePointAt(0));
  }
  const ji = integer(i, j);
  return function () {
    return String.fromCodePoint(ji());
  };
}

function array(first, value) {
  if (Array.isArray(first)) {
    return function () {
      return first.map(resolve);
    };
  }
  if (first === undefined) {
    first = integer(4);
  }
  if (value === undefined) {
    value = integer();
  }
  return function () {
    const dimension = resolve(first);
    const result = new Array(dimension).fill(value);
    return typeof value === "function" ? result.map(resolve) : result;
  };
}

function string(...parameters) {
  const length = parameters.length;
  if (length === 0) {
    return string(integer(10), character());
  }

  return function () {
    let pieces = [];
    let parameterIndex = 0;
    let value;
    while (true) {
      value = resolve(parameters[parameterIndex]);
      parameterIndex += 1;
      if (value === undefined) {
        break;
      }
      if (
        Number.isSafeInteger(value) &&
        value >= 0 &&
        parameters[parameterIndex] !== undefined
      ) {
        pieces = pieces.concat(
          new Array(value).fill(parameters[parameterIndex]).map(resolve),
        );
        parameterIndex += 1;
      } else {
        pieces.push(String(value));
      }
    }
    return pieces.join("");
  };
}
const threeWordSpecifier = string(
  sequence(["c", "d", "f"]),
  sequence(["a", "o", "i", "e"]),
  sequence(["t", "g", "n", "s", "l"]),
);
// console.log(threeWordSpecifier());
// console.log(threeWordSpecifier());
// console.log(threeWordSpecifier());
// console.log(threeWordSpecifier());
// console.log(threeWordSpecifier());

const misc = [
  true,
  Infinity,
  -Infinity,
  falsy(),
  Math.PI,
  Math.E,
  Number.EPSILON,
];
function any() {
  return oneOf([integer(), number(), string(), oneOf(misc)]);
}

// `object` returns a generator that returns objects
// the default is to make small objects with random keys and random values
function object(subject, value) {
  if (subject === undefined) {
    subject = integer(1, 4);
  }
  return function () {
    let result = {};
    const keys = resolve(subject);
    if (typeof keys === "number") {
      const text = string();
      const gen = any();
      let i = 0;
      while (i < keys) {
        result[text()] = gen();
        i += 1;
      }
      return result;
    }
    if (value === undefined) {
      if (keys && typeof keys === "object") {
        Object.keys(subject).forEach((key) => {
          result[key] = resolve(keys[key]);
        });
        return result;
      }
    } else {
      const values = resolve(value);
      if (Array.isArray(keys)) {
        keys.forEach((key, keyIndex) => {
          result[key] = resolve(
            Array.isArray(values) ? values[keyIndex % values.length] : value,
            keyIndex,
          );
        });
        return result;
      }
    }
  };
}

const ctp = "{name}: {class}{class} cases tested, {pass} pass{fail}{lost}\n";

// the `crunch` function crunches the numbers and prepares the reports
function crunch(detail, cases, serials) {
  let classFail;
  let classPass;
  let classLost;
  let caseNumber = 0;
  let lines = "";
  let losses = [];
  let nextCase;
  let nowClaim; // to be tested?
  let numberClass = 0;
  let numberFail;
  let numberLost;
  let numberPass;
  let report = "";
  let theCase;
  let theClass;
  let totalFail = 0;
  let totalLost = 0;
  let totalPass = 0;

  function generateLine(type, level) {
    if (detail >= level) {
      lines += fulfuill(" {type} [{serial}] {classification}{args}\n", {
        type,
        serial: theCase.serial,
        classification: theCase.classification,
        args: JSON.stringify(theCase.args)
          .replace(/^\[/, "(")
          .replace(/\]$/, ")"),
      });
    }
  }

  function generateClass(key) {
    if (detail >= 3 || classFail[key] || classLost[key]) {
      report += fulfuill(" {key} pass {pass}{fail}{lost}\n", {
        key,
        pass: classPass[key],
        fail: classFail[key] ? " fail " + classFail[key] : "",
        lost: classLost[key] ? " lost " + classLost[key] : "",
      });
    }
  }

  if (cases) {
    while (true) {
      nextCase = cases[serials[caseNumber]];
      caseNumber += 1;
      if (!nextCase || nextCase.claim !== nowClaim) {
        if (nowClaim) {
          if (detail >= 1) {
            report += fulfuill(ctp, {
              name: theCase.name,
              class: numberClass ? numberClass + " classifications, " : "",
              cases: numberPass + numberFail + numberLost,
              pass: numberPass,
              fail: numberFail ? ", " + numberFail + " fail" : "",
              lost: numberLost ? ", " + numberLost + " lost" : "",
            });
            if (detail >= 2) {
              Object.keys(classPass).sort().forEach(generateClass);
              report += lines;
            }
          }
          totalFail += numberFail;
          totalLost += numberLost;
          totalPass += numberPass;
        }
        if (!nextCase) {
          break;
        }
        numberClass = 0;
        numberFail = 0;
        numberLost = 0;
        numberPass = 0;
        classPass = {};
        classFail = {};
        classLost = {};
        lines = "";
      }
      theCase = nextCase;
      nowClaim = theCase.claim;
      theClass = theCase.classification;

      if (theClass && typeof classPass[theClass] !== "number") {
        classPass[theClass] = 0;
        classFail[theClass] = 0;
        classLost[theClass] = 0;
        numberClass += 1;
      }
      if (theCase.pass === true) {
        if (theClass) {
          classPass[theClass] += 1;
        }
        if (detail >= 4) {
          generateLine("Pass", 4);
        }
        numberPass += 1;
      } else if (theCase.pass === false) {
        if (theClass) {
          classFail[theClass] += 1;
        }
        generateLine("FAIL", 2);
        numberFail += 1;
      } else {
        // lost
        if (theClass) {
          classLost[theClass] += 1;
        }
        generateLine("LOST", 2);
        losses[numberLost] = theCase;
        numberLost += 1;
      }
    }

    report += fulfuill("\nTotal pass {pass}{fail}{lost}\n", {
      pass: totalPass,
      fail: totalFail ? ", fail " + totalFail : "",
      lost: totalLost ? ", lost " + totalLost : "",
    });
  }

  return {
    losses,
    report,
    summary: {
      pass: totalPass,
      fail: totalFail,
      lost: totalLost,
      total: totalPass + totalFail + totalLost,
      ok: totalLost === 0 && totalFail === 0 && totalPass > 0,
    },
  };
}

// the module exports a constructor that returns a `jsc` object
// a `jsc` object is stateful because it holds the claims to be tested

// `reject` is used to identify trials that should be rejected
const reject = Object.freeze({});

// exports a `jscConstructor` function
// `check` and `claim` functions are stateful
export default Object.freeze(function jscConstructor() {
  let allClaims = [];

  // `check` is what does the work
  // actually runs the tests I assume?
  function check(configuration) {
    let theClaims = allClaims;
    allClaims = [];
    let numberTrials =
      configuration.numberTrials === undefined
        ? 100
        : configuration.numberTrials;

    function go(on, report) {
      try {
        return configuration[on](report);
      } catch (ignore) {}
    }

    // the `check` function checks all claims
    // the results are provided to callback functions
    let cases = {};
    let allStarted = false;
    let numberPending = 0;
    let serials = [];
    let timeoutId;

    function finish() {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }

      const { losses, summary, report } = crunch(
        configuration.detail === undefined ? 3 : configuration.detail,
        cases,
        serials,
      );
      losses.forEach((theCase) => go("onLost", theCase));
      go("onResult", summary);
      go("onReport", report);
      cases = undefined;
    }

    // the `register` function is used by a claim function to register a new
    // case
    // also used by a case to report a verdict
    // the two use cases are correlated by the serial number
    function register(serial, value) {
      if (cases) {
        let theCase = cases[serial];

        // if serial number not seen before, then register a new case
        // the case is added to the cases collection
        if (theCase === undefined) {
          value.serial = serial;
          cases[serial] = value;
          serials.push(serial);
          numberPending += 1;
        } else {
          // an existing case now gets its verdict
          // if it unexpectedly already has a result, then throw an exception
          // each case should have only one result
          if (theCase.pass !== undefined || typeof value !== "boolean") {
            throw theCase;
          }

          if (value === true) {
            theCase.pass = true;
            go("onPass", theCase);
          } else {
            theCase.pass = false;
            go("onFail", theCase);
          }

          // the case is no longer pending
          // if all cases have been generated and given results, then finish
          numberPending -= 1;
          if (numberPending <= 0 && allStarted) {
            finish();
          }
        }
      }

      return value;
    }

    let unique = 0;

    // process each claim
    theClaims.forEach((aClaim) => {
      let atMost = numberTrials * 10;
      let caseNumber = 0;
      let attemptNumber = 0;

      // loop over the generation and testing of cases
      while (caseNumber < numberTrials && attemptNumber < atMost) {
        // this `aClaim` corresponds to the `theClaim` function
        if (aClaim(register, unique) !== reject) {
          caseNumber += 1;
          unique += 1;
        }
        attemptNumber += 1;
      }
    });

    // all case predicates have been called
    allStarted = true;
    if (numberPending <= 0) {
      finish();
    } else if (configuration.timeLimit !== undefined) {
      timeoutId = setTimeout(finish, configuration.timeLimit);
    }
  }

  // the `claim` function is used to file each claim
  // all claims are checked at once when the `check` function is called
  // A claim consists of
  //   - a descriptive name that's displayed in the report
  //   - a predicate function that exercises the claim and returns `true` if
  //   the claim holds
  //   - a function signature array that specifies the types and values for the
  //   predicate function
  //   - an optional classifier function that takes values produced by the
  //   signature and that returns a string for classifying the trials, or
  //   `undefined` if the predicate should not be given this set of generated
  //   arguments
  function claim(name, predicate, signature, classifier) {
    if (!Array.isArray(signature)) {
      signature = [signature];
    }

    function theClaim(register, serial) {
      let args = signature.map(resolve);
      let classification = "";

      // if a classifier function was provided, then use it to obtain
      // a classification
      if (classifier !== undefined) {
        classification = classifier(...args);
        if (typeof classification !== "string") {
          return reject;
        }
      }

      // create a verdict function that wraps the register function
      let verdict = function (result) {
        return register(serial, result);
      };

      // register an object that represents this trial
      register(serial, {
        args,
        claim: theClaim,
        classification,
        classifier,
        name,
        predicate,
        serial,
        signature,
        verdict,
      });

      // call the predicate
      // the predicate must use the verdict callback to signal the result of
      // the case
      return predicate(verdict, ...args);
    }
    allClaims.push(theClaim);
  }

  return Object.freeze({
    any,
    array,
    boolean,
    character,
    falsy,
    integer,
    literal,
    number,
    object,
    oneOf,
    sequence,
    string,

    // main functions:
    check,
    claim,
  });
});
