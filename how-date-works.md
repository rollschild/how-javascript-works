# How Date Works

- A better leap year algorithm:
  > Add a leap day in years divisible by 4, unless they are also divisible by 128
- Minutes, seconds, and hours are all zero origin values
- Months and days are one origin values because they were standardized before
  zero was discovered
- **Common Era** epoch
- `getMonth` is zero origin
- `getYear` and `setYear` do **NOT** work correctly after 1999
- **ALWAYS** use `getFullYear` and `setFullYear` instead
- `Date` is a bad practice in classical programming
  - an object should encapsulate something
  - interactions with objects should be transactions and other high level
    activities
  - `Date` is exposing a very low level view with getters and setters for each
    individual component of time
- `new Date` constructor can take a string and produce an object for that date
  - for a random string, it's **NOT** guaranteed to work
  - except for ISO dates
- ISO date
  - `2022-03-20`
- JavaScript uses the Unix epoch: `1970-01-01`
  - it will fail in year 2038, running out of 32-bits
