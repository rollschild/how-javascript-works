# How `this` works

- `const newObject = Object.create(oldProject)`
- it's possible for many objects to share a prototype
- when a method is called, the function called also receives `this` as an
  implied parameter bound to the object of interest
- if a method contains an inner function, the inner function does **NOT** get
  access to `this` because inner functions are called as functions
  - **ONLY** method calls get the `this` binding

```javascript
oldObject.bud = function bud() {
  const that = this;
  function lou() {
    doItTo(that);
  }
  lou();
};

newObject.bud(); // succeeds

const funky = newObject.bud;
funky(); // FAILS! because it's called as a function
```

- `this` is bound **dynamically**

```javascript
function pubsub() {
  const subscribers = [];
  return {
    subscribe: function (subscriber) {
      subscribers.push(subscriber);
    },
    publish: function (publication) {
      const length = subscribers.length;
      for (let i = 0; i < length; i += 1) {
        subscribers[i](publication);
      }
    },
  };
}
```

- a subscriber function gets a `this` binding to the `subscribers` array
  because `subscribers[i](publication)` is a method call, which allows for some
  bad things:

```javascript
myPubsub.subscribe(function (publication) {
  this.length = 0;
});
```

- To fix this, we can replace `for` loop with `forEach()`

```javascript
{
  publish: function (publication) {
    subscribers.forEach(function (subscriber) {
      subscriber(publication);
    })
  }
}
```

- a function object has two prototype properties
  - a delegation link to `Function.prototype`
  - a `prototype` property that contains a reference to an object that is used
    as the prototype of objects constructed by the function when it's called
    with the `new` prefix
- A constructor call is written by placing `new` in front of a function call;
  it does the following:
  - make a `this` value with `Object.create(function.prototype)`
  - call the function with `this` bound to the new object
  - if the function does not return an object, force it to return `this`
- Each function is a potential constructor
  - it's difficult to know at any instant if a call should use a `new` prefix
- Convention:
  - Functions that are intended to be called as constructors with the `new`
    prefix must be named with initial CAPS
  - nothing else should be written with initial caps
