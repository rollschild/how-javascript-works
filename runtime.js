import BigFloat from "./big-floating-point.js";
import BigInteger from "./big-integer.js";

function fail(what = "fail") {
  throw new Error(what);
}

// the only stateful variable is `weakmapOfWeakmaps`
// it binds weakmaps to records

// the `get` function implements call-a-function-as-a-method by returning
// a function

let weakmapOfWeakmaps = new WeakMap();
function get(container, key) {
  try {
    if (Array.isArray(container) || typeof container === "string") {
      const elementNumber = BigFloat.number(key);
      return Number.isSafeInteger(elementNumber)
        ? container[
            elementNumber >= 0
              ? elementNumber
              : container.length + elementNumber
          ]
        : undefined;
    }
    if (typeof container === "object") {
      if (BigFloat.isBigFloat(key)) {
        key = BigFloat.string(key);
      }
      return typeof key === "string"
        ? container[key]
        : weakmapOfWeakmaps.get(container).get(key);
    }
    if (typeof container === "function") {
      return function (...rest) {
        return container(key, rest);
      };
    }
  } catch (ignore) {}
}

// the `get` function can be accessed as the `f[]` functino

function set(container, key, value) {
  if (Object.isFrozen(container)) {
    return fail("set");
  }
  if (Array.isArray(container)) {
    // uses only big floats for keys
    let elementNumber = BigFloat.number(key);
    if (!Number.isSafeInteger(elementNumber)) {
      return fail("set");
    }
    if (elementNumber < 0) {
      elementNumber = container.length + elementNumber;
    }
    if (elementNumber < 0 || elementNumber >= container.length) {
      return fail("set");
    }
    container[elementNumber] = value;
  } else {
    if (BigFloat.isBigFloat(key)) {
      key = BigFloat.string(key);
    }

    // if the key is a string, then it's an object update
    if (typeof key === "string") {
      if (value === undefined) {
        delete container[key];
      } else {
        container[key] = value;
      }
    } else {
      // a weakmap update
      // there will be a weakmap associated with each record with object keys
      if (typeof key !== "object") {
        return fail("set");
      }

      let weakmap = weakmapOfWeakmaps.get(container);
      if (weakmap === undefined) {
        // make one
        if (value === undefined) {
          return;
        }
        weakmap = new WeakMap();
        weakmapOfWeakmaps.set(container, weakmap);
      }

      // update the weakmap
      if (value === undefined) {
        weakmap.delete(key);
      } else {
        weakmap.set(key, value);
      }
    }
  }
}

// make a new Array, fill, slice, Object.keys, split, and more
function array(zeroth, oneth, ...rest) {
  if (BigFloat.isBigFloat(zeroth)) {
    const dimension = BigFloat.number(zeroth);
    if (!Number.isSafeInteger(dimension) || dimension < 0) {
      return fail("array");
    }
    let newness = new Array(dimension);
    return oneth === undefined || dimension === 0
      ? newness
      : typeof oneth === "function"
      ? newness.map(oneth)
      : newness.fill(oneth);
  }
  if (Array.isArray(zeroth)) {
    if (typeof oneth === "function") {
      return zeroth.map(oneth);
    }
    return zeroth.slice(BigFloat.number(oneth), BigFloat.number(rest[0]));
  }
  if (typeof zeroth === "object") {
    return Object.keys(zeroth);
  }
  if (typeof zeroth === "string") {
    return zeroth.split(oneth || "");
  }
  return fail("array");
}

function number(a, b) {
  return typeof a === "string"
    ? BigFloat.make(a, b)
    : typeof a === "boolean"
    ? BigFloat.make(Number(a))
    : BigFloat.isBigFloat(a)
    ? a
    : undefined;
}

function record(zeroth, oneth) {
  const newness = Object.create(null);
  if (zeroth === undefined) {
    return newness;
  }
  if (Array.isArray(zeroth)) {
    if (oneth === undefined) {
      oneth = true;
    }
    zeroth.forEach((element, elementNumber) => {
      set(
        newness,
        element,
        Array.isArray(oneth)
          ? oneth[elementNumber]
          : typeof oneth === "function"
          ? oneth(element)
          : oneth,
      );
    });
    return newness;
  }
  if (typeof zeroth === "object") {
    if (oneth === undefined) {
      return Object.assign(newness, zeroth);
    }
    if (typeof oneth === "object") {
      return Object.assign(newness, zeroth, oneth);
    }
    if (Array.isArray(oneth)) {
      oneth.forEach((key) => {
        let value = zeroth[key];
        if (value !== undefined) {
          newness[key] = value;
        }
      });
      return newness;
    }
  }
  return fail("record");
}

function text(zeroth, oneth, twoth) {
  if (typeof zeroth === "string") {
    return zeroth.slice(BigFloat.number(oneth), BigFloat.number(twoth));
  }

  // number to string
  // with radix
  if (BigFloat.isBigFloat(zeroth)) {
    return BigFloat.string(zeroth, oneth);
  }

  // join
  if (Array.isArray(zeroth)) {
    let separator = oneth;
    if (typeof oneth !== "string") {
      if (oneth !== undefined) {
        return fail("string");
      }
      separator = "";
    }
    return zeroth.join(separator);
  }

  if (typeof zeroth === "boolean") {
    return String(zeroth);
  }
}

// `stone` is deep freeze
function stone(object) {
  if (!Object.isFrozen(object)) {
    object = Object.freeze(object);

    // now deep freeze
    if (typeof object === "object") {
      if (Array.isArray(object)) {
        object.forEach(stone);
      } else {
        Object.keys(object).forEach((key) => {
          stone(object[key]);
        });
      }
    }
  }
  return object;
}

// predicate functions that are used to identify the types of things
function boolean_(any) {
  return typeof any === "boolean";
}
function function_(any) {
  return typeof any === "function";
}
function integer_(any) {
  return BigFloat.isBigFloat(any) && BigFloat.normalize(any).exponent === 0;
}
function number_(any) {
  return BigFloat.isBigFloat(any);
}
function record_(any) {
  return any !== null && typeof any === "object" && !BigFloat.isBigFloat(any);
}
function text_(any) {
  return typeof any === "string";
}

// functino versions of logical functions
// they are **NOT** short-circuting
function assertBoolean(boolean) {
  return typeof boolean === "boolean" ? boolean : fail("boolean");
}

function and(zeroth, oneth) {
  return assertBoolean(zeroth) && assertBoolean(oneth);
}

function or(zeroth, oneth) {
  return assertBoolean(zeroth) || assertBoolean(oneth); // why?
}

function ternary(zeroth, oneth, twoth) {
  return assertBoolean(zeroth) ? oneth : twoth;
}

function defaultFunction(zeroth, oneth) {
  return zeroth === undefined ? oneth : zeroth;
}

// group of relational operators
function eq(zeroth, oneth) {
  return (
    zeroth === oneth ||
    (BigFloat.isBigFloat(zeroth) &&
      BigFloat.isBigFloat(oneth) &&
      BigFloat.eq(zeroth, oneth))
  );
}

function lt(zeroth, oneth) {
  return zeroth === undefined
    ? false
    : oneth === undefined
    ? true
    : BigFloat.isBigFloat(zeroth) && BigFloat.isBigFloat(oneth)
    ? BigFloat.lt(zeroth, oneth)
    : typeof zeroth === typeof oneth &&
      (typeof zeroth === "string" || typeof zeroth === "number")
    ? zeroth < oneth
    : fail("lt");
}

function ge(zeroth, oneth) {
  return !lt(zeroth, oneth);
}

function gt(zeroth, oneth) {
  return lt(oneth, zeroth);
}

function le(zeroth, oneth) {
  return !lt(oneth, zeroth);
}

function ne(zeroth, oneth) {
  return !eq(oneth, zeroth);
}

// group of arithmetic operators
function add(a, b) {
  return BigFloat.isBigFloat(a) && BigFloat.isBigFloat(b)
    ? BigFloat.add(a, b)
    : undefined;
}

function sub(a, b) {
  return BigFloat.isBigFloat(a) && BigFloat.isBigFloat(b)
    ? BigFloat.subtract(a, b)
    : undefined;
}

function mul(a, b) {
  return BigFloat.isBigFloat(a) && BigFloat.isBigFloat(b)
    ? BigFloat.multiply(a, b)
    : undefined;
}

function div(a, b) {
  return BigFloat.isBigFloat(a) && BigFloat.isBigFloat(b)
    ? BigFloat.div(a, b)
    : undefined;
}

function max(a, b) {
  return lt(b, a) ? a : b;
}

function min(a, b) {
  return lt(a, b) ? a : b;
}

function abs(a) {
  return BigFloat.isBigFloat(a) ? BigFloat.abs(a) : undefined;
}

function fraction(a) {
  return BigFloat.isBigFloat(a) ? BigFloat.fraction(a) : undefined;
}

function integer(a) {
  return BigFloat.isBigFloat(a) ? BigFloat.integer(a) : undefined;
}

function neg(a) {
  return BigFloat.isBigFloat(a) ? BigFloat.negate(a) : undefined;
}

// group of bitwise functions
// done by BigInteger
function bitand(a, b) {
  return BigFloat.make(
    BigInteger.and(
      BigFloat.integer(a).coefficient,
      BigFloat.integer(b).coefficient,
    ),
    BigInteger.one,
  );
}

function bitdown(a, numberOfBits) {
  return BigFloat.make(
    BigInteger.shiftDown(
      BigFloat.integer(a).coefficient,
      BigFloat.number(numberOfBits),
    ),
    BigInteger.one,
  );
}

function bitmask(numberOfBits) {
  return BigFloat.make(BigInteger.mask(BigFloat.number(numberOfBits)));
}

function bitor(a, b) {
  return BigFloat.make(
    BigInteger.or(
      BigFloat.integer(a).coefficient,
      BigFloat.integer(b).coefficient,
    ),
    BigInteger.one,
  );
}

function bitup(a, numberOfBits) {
  return BigFloat.make(
    BigInteger.shiftUp(
      BigFloat.integer(a).coefficient,
      BigFloat.number(numberOfBits),
    ),
    BigInteger.one,
  );
}

function bitxor(a, b) {
  return BigFloat.make(
    BigInteger.xor(
      BigFloat.integer(a).coefficient,
      BigFloat.integer(b).coefficient,
    ),
    BigInteger.one,
  );
}

function resolve(value, ...rest) {
  return typeof value === "function" ? value(...rest) : value;
}

function cat(zeroth, oneth) {
  zeroth = text(zeroth);
  oneth = text(oneth);
  if (typeof zeroth === "string" && typeof oneth === "string") {
    return zeroth + oneth;
  }
}

function cats(zeroth, oneth) {
  zeroth = text(zeroth);
  oneth = text(oneth);
  if (typeof zeroth === "string" && typeof oneth === "string") {
    return zeroth === "" ? oneth : oneth === "" ? zeroth : zeroth + " " + oneth;
  }
}

function char(any) {
  return String.fromCodePoint(BigFloat.number(any));
}

function code(any) {
  return BigFloat.make(any.codePointAt(0));
}

function length(linear) {
  return Array.isArray(linear) || typeof linear === "string"
    ? BigFloat.make(linear.length)
    : undefined;
}

export default stone({
  abs,
  add,
  and,
  array,
  assertBoolean,
  bitand,
  bitdown,
  bitmask,
  bitor,
  bitup,
  bitxor,
  boolean_,
  cat,
  cats,
  char,
  code,
  default: defaultFunction,
  div,
  eq,
  fail,
  fraction,
  function_,
  ge,
  get,
  gt,
  integer,
  integer_,
  le,
  length,
  max,
  min,
  mul,
  ne,
  neg,
  not,
  number,
  number_,
  or,
  record,
  record_,
  resolve,
  set,
  stone,
  sub,
  ternary,
  text,
  text_,
});
