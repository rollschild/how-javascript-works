# How Parsing Works

- To parse, we weave the stream of token objects into a tree
- The token objects are augmented with new properties
  - the `zeroth`, `oneth`, and `twoth` properties
  - they give the tree structure
- report errors with the `error` function
  - stop after the first error
