# How Bottom Values Work

- Bottom values are special values that indicate the end of a recursive data
  structure or absence of a value
- Two bottom values in JavaScript: `null` and `undefined`
  - `NaN` could also be one
- `null` and `undefined` are the only values in JS that are not objects
- **We should eliminate `null` and use `undefined` exclusively**
  - because `undefined` is the value JS itself uses
- the only place `null` should be used is in `Object.create(null)`
- `typeof null === "object"`
- `typeof undefined === "undefined"`
