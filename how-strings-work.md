# How Strings Work

- An **immutable** array of **16-bit** unsigned integers from `0` thru `65535`
- `String.fromCharCode()`
- `charCodeAt()`
- `startsWith()`
- `endsWith()`
- `lastIndexOf()`

## Unicode

- all strings are frozen when they are created
- `padStart()`
- Unicode task JavaScript's _character_ and breaks it into two new terms: **code unit** and **code point**
  - **code unit**:
    - one of those 16-bits characters
  - **code point**:
    - the number that corresponds to a character
  - a code point is formed from 1 or more code units
- Unicode defines 1,114,112 code points
  - divided into 17 planes of 65,536 code points
  - the original plane is now called **Basic Multilingual Plane (BMP)**
- JS accesses supplemental chars by use of **surrogate pairs**
  - a **surrogate pair** is formed by two special code units
  - 1024 high surrogate code units
  - 1024 low surrogate code units
- When properly paired, each surrogate code unit contributes 10 bits to form a 20-bit offset, to which 65,536 is added
  to form the code point
  - numeric addition
- from JavaScript's point of view, `U+1F4A9` is two 16-bit characters
  - it will display as a single char if the OS is able to
- Two ways to write the code point `U+1F4A9` with escapement in a string literal
  - `"\uD83D\uDCA9"`
  - `"\u{1F4A9}"`
  - they both produce the same string with length of 2
- `String.fromCodePoint()` - make surrogate pairs

```javascript
String.fromCharCode(55357, 56489) === String.fromCodePoint(128169);
```

- `String.codePointAt()` similar to `String.charCodeAt()`, except that it may also look at the next character, and if
  they form a surrogate pair, it returns a supplemental code point

```javascript
"\uD83D\uDCA9".codePointAt(0) === 128169;
```

- Unicode has normalization rules that specify the order in which things must appear and when base characters with
  modifiers should be replaced with composite chars
  - `String.normalize()`

## Template String Literals

- Template string literals are string literals that can span multiple lines
  - using <code>\`</code>
- In most cases, long form textual material should **NOT** be stored in programs
- template string literals with interpolation are **unsafe** by default
- **tag** function - one way to mitigate
  - preceding a template string literal with a _function expression_
  - it causes the function to be called with the template string and the values of the expressions as its arguments
  - mitigates XSS exploits

## Regular Expressions

- string methods that can take regex as arguments:
  - `match`
  - `replace`
  - `search`
  - `split`
- regex objects that can take strings as arguments
  - `exec`
  - `test`
- Regular expressions are **not** powerful enough to parse JSON text, but can be used to break JSON text into tokens

## Tokenization

- JavaScript is a very difficult language to tokenize, because of:
  - interaction between regular expression literals and automatic semicolon insertion

```javascript
return /a/i;
return b.return / a / i;
```

- `/(?:)/` - non-capturing group
  - group multiple tokens together without creating a capturing group
