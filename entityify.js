// makes text safe for insertion into HTML
function entityify(text) {
  return text
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/\\/g, "&bsol;")
    .replace(/"/g, "&quot;");
}
const script = "<script src=https://enemy.evil/pwn.js />";
console.log(entityify(script));
