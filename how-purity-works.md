# How Purity Works

- **Functional programming** is defined as **programming with functions**
- A pure function does not cause mutation
- A pure function is not influenced by mutation
- A pure function's result is determined only by its inputs
- A pure function performs no action other than to provide an output
- A given output always produces the same input
- Pure functions can make threads safe and efficient
- Parallelization of pure functions!
- The universe is eventual, fully distributed, and highly parallel
-
