# How Numbers Work

- JS has a single number type: `number`
- floating point:
  - represent a number as two numbers
  - first number contains the digits
  - second number (a.k.a. exponent) identifies where the decimal point (or
    binary point) should be inserted in the first number
- the `number` type is a 64-bit binary floating point type
- a number contains:
  - a sign bit
  - 11 exponent bits
  - 53 significand bits
- some encoding allows the 65 bits to be packed into a 64-bit word
- first number is split into two parts
  - the sign, placed in the most significant of the 64 bits
    - `1` if negative
  - the significand, placed in the least significant bits
    - normalized binary number
    - a.k.a. **mantissa** or **coefficient**
    - normally represents a binary fraction in the range `0.5 <= significand < 1.0`
    - in this case the most significant bit is always `1`
    - doesn't need to be stored in the number
- second number is the exponent
- value of a number:

```math
sign * significand * (2 ** exponent)
```

- the exponent is represented as a biased signed magnitude integer
- the exponent can also encode `NaN` and `Infinity` and subnormals
- `Infinity`:
  - all `1`s for the exponent and all `0`s for the significand
- `NaN`:
  - all `1`s for the exponent and anything **but** all `0`s for the significand
- `Number.MAX_SAFE_INTEGER`
  - all `1`s for the significand

## Zero

- the IEEE 754 standard contains two zeros: `0` and `-0`

```js
1 / 0 === 1 / -0; // false
Object.is(0, -0); // false
```

- `Object.is()`
  - check whether two values are the same value
  - not the same as `==` or `===`
  - `==` and `===` treat `0` and `-0` as equal but `Object.is()` does not
  - `===` treats `Number.NaN` and `NaN` as **not** equal

## Number Literals

- there are 18437736874454810627 _immutable_ number objects built into JavaScript
  each uniquely represents a number
- a number literal produces a reference to the number object that most closely
  matches the value of the literal

- `Math.pow(2, 1024) = Infinity`
- `NaN`: not a number
  - `NaN !== NaN`
  - use `Number.isNaN(value)` instead
- `Number.isFinite(value)`

## Number

- `Number` is a function that can make numbers
- Numbers in JavaScript are immutable objects
- **NEVER** use the `new` prefix with the `Number()` function
- `Number` is a container of some constants
  - `Number.EPSILON`
    - `2 ^ -52`
    - difference between `1` and the smallest floating point number greater
      than `1`
    - adding any positive number less than `Number.EPSILON` to `1` produces
      a sum exactly equal `1`
  - `Number.MAX_SAFE_INTEGER`
    - JS doesn't need/have to have an integer type
      - its number type can exactly represent all integers up to
        `Number.MAX_SAFE_INTEGER`
      - JS has 54-bit signed integers within its number type
  - `Number.isSafeInteger`
  - `Number.isInteger` can return true for integers even if they are larger
    than `Number.MAX_SAFE_INTEGER`
  - `Number.MAX_VALUE` - largest number that JS can represent
    - exactly `Number.MAX_SAFE_INTEGER * 2 ** 971`
    - adding any positive safe integer to `Number.MAX_VALUE` is also
      `Number.MAX_VALUE`
  - `Number.MIN_VALUE`
    - smallest number that can be represented that is larger than zero
    - `2 ** 1074`
    - any positive number smaller than `Number.MIN_VALUE` is indistinguishable
      from zero

## Operators

- Two types of operators
  - prefix
    - `+`
      - converts its operand into a number
      - if conversion fails, then `NaN`
      - function `Number()` is preferred
    - `-`
      - changes sign of its operand
      - Number literals in JS do **NOT** have signs
    - `typeof`
      - returns `"number"` even for `NaN`
  - infix
    - `+`
      - also used for concatenating strings
      - if either of the operands is a string, it converts the other operand to
        string
    - `-`
    - `*`
    - `/`
      - **NOT** an integer division
    - `%`
      - remainder, **NOT** modulo
      - result of remainder takes its sign from dividend
      - result of modulo takes its sign from the divisor
    - `**`
      - from FORTRAN

## Bitwise Operators

- they all operate on JS numbers by converting them to signed 32-bit ints,
  performing the bitwise operation, then converting back to JS numbers
- they do **NOT** operate on 54-bit safe integers!
  - the 22 high order bits can be lost without warning
- `<<`: left shift
- `>>>`: right shift (unsigned)
  - results always non-negative
- `>>`: right shift sign extended

## The Math Object

- `Math.floor` vs. `Math.trunc`
  - `Math.floor` gives the smallest integer
  - `Math.trunc` gives the integer closest to zero
