# How Booleans Work

- `typeof true === "boolean";`

```javascript
NaN === NaN; // false
NaN !== NaN; // true
```

- **ALWAYS** use `Number.isNaN()` to determine if a value is `NaN`
- To test for the completion of a loop
  - use `>=` or `<=`
  - do **NOT** use `===`
- Falsy values:
  - `false`
  - `null`
  - `undefined`
  - `""`
  - `0`
  - `NaN`
- `!!p === p` **only** true if `p` is a boolean
  - if `p` is of any other type, `!!p` equals to `Boolean(p)`
- De Morgan Law

```javascript
!(p && q) === !p || !q;
!(p || q) === !p && !q;
```
