# How Big Floating Point Works

- A floating point system is concerned with three numbers
  - **coefficient**
  - **exponent**
  - **basis**

```javascript
value = coefficient * basis ** exponent;
```

-
