# How Transpiling Works

- **Transpiling** is a specialized form of compiling in which one programming
  language is transformed into another
  - in this case JavaScript

## Neo

- No reserved words
- Names can have spaces; names can end with `?`
- Comments are made with `#`; a comment ends at the end of a line
- Semicolon free with meaningful whitespace
- Fewer precedence levels; they are:

  0. `?` `!` `||`
  1. `/\`_logical and_ `\/`_logical or_
  2. `=` `!=` `<` `>` `<=` `>=`
  3. `~`_concatenate_ `~~`_concatenate with space_
  4. `+` `-` `<<`_minimum_ `>>`_maximum_
  5. `*` `/`
  6. `.`_refine_ `[ ]` `( )`

- `null` unifies `null`, `undefined`, and `NaN` into a single object
  - an empty immutable object
  - getting a property from `null` does not fail, it just produces `null`
- No prefix operators
- One number type: Big Decimal
  - no need for `MIN_VALUE`, `EPSILON`, `MAX_SAFE_INTEGER`, nor `Infinity`
- A sequence of characters is called _text_
- A data type called **record** that unifies objects and weakmaps
  - record does not inherit
  - a container of fields
  - a field has a key and value
  - a value can be any value except `null`
    - setting the value to `null` deletes the field from the record
- `array(dimension)`
  - initializes the array to `null`
- `stone` function replaces the `Object.freeze()` function
  - deep freeze
- ternary operator uses `?` and `!`
- `not`
  - if `/\`, `\/`, or `not` are given non-boolean values, they fail
- unary arithmetic functions:
  - `abs`
  - `fraction`
  - `integer`
  - `neg`
- `~` and `~~` attempt to coerce their operands into texts
- `typeof` is replaced with predicate functions
  - `array?`
  - `boolean?`
  - `function?`
  - `number?`
  - `record?`
  - `text?`
- `Number.isInteger` and `Number.isSafeInteger` are replaced with `integer?`
- `Object.isFronzen` replaced with `stone?`
- `char` function takes a code point and returns a text
- `code` takes a text and returns the first code point
- `length` takes an array or text and returns the number of elements/characters
- `array` function makes an array
- `number` function takes a text and returns a number
  - can take an optional radix
- `record` function makes a record
  - if the argument is an array, then use the elements of the array as keys; the values depend on the other argument:
    - if `null`, all values are `true`
      - a.k.a. `set` sometimes
    - if an array, its elements are the values; the arrays should have the same length
    - if a function then it's used to generate the values
    - otherwise, the argument is used as the initial value of all fields
  - if the argument is a record, then make a shallow copy of the record, copying only the text keys
    - if there is another argument that is an array of keys, then only those keys will be copied
  - if the argument is `null`, then make an empty record
- `text` function makes a text; depends on its argument
  - if a number, convert to text, can take optional radix
  - if an array, concatenate all elements together
    - another argument can provide separator text
  - if a text, tow additional arguments can specify a subtext
- function objects are made with `f` in two forms:

```
f parameter list (expression)
```

```
f parameter list {
  function body
}
```

- _parameter list_ is a `,` separated list of names and optional defaults and `...`
- functions are anonymous
- to give functions names, use the `def` statements
- function objects are _immutable_
- a function expression returns the value of the expression
- a function body must explicitly return a value
- the default operator is `|expression|`
  - if the expression/parameter immediately to the left has a value of `null`, then the `expression` wrapped in `||` on
    the right is evaluated to produce a default value
    - it's short circuiting
- `...` is _always_ placed _after_ the array term

```
def continuize: f any (
    f callback, arguments... (callback(any(arguments...)))
)
```

- Using `f` as a prefix before an operator produces a **functino**
  - allows the use of the operator as an ordinary function
  - `f+` makes a binary add function that can be passed to a reduce function to make sums
  - `f+(3, 4)` returns `7`
- a function can be called as a method
  - this allows to use a function as a proxy to a record

```
my function.method(x, y)
# does the same thing as
my function("method", [x, y])
```

- `def` replaces `const`
- `var` declares variables
- `let` statement can change the value of a variable, or a field of a record, or an element of an array
  - the `let` statement is the _only_ place where mutation is allowed
- No assignment operators

```
def pi: 3.141592653589793
var counter: 0
let counter: counter + 1
```

- No block scope because there are no blocks
- There is function scope
- `call` statement allows for calling a function but ignoring its return value

```
call my impure side effect causer()
```

- No falsy values
- `loop` statement replaces the `do`, `for`, and `while` statements
  - looping is discouraged
  - use `break` or `return` to exit
  - loops may be nested
  - new functions **CANNOT** be made in loops
- Exceptions are replaced with failures
  - a function can have a failure handler
  - No `try`

```
def my function: f x the unknown {
    return risky operation(x the unknown)
failure
    call launch all missiles()
    return null
}
```

- `fail` statement signals a failure
  - does **not** take an exception object or any other sort of message
- a module can have several `import` statements

```
import name: text literal
```

- a module can have only one `export`

```
export expression
```

## The Next Language

- Use UTF-32 as the internal character representation for the next language!
  - with UTF-32, the difference between code units and code points vanishes
- Should have direct support for blobs
- Should have better support for eventual programming, including
  - a processing loop
  - a mechanism for message dispatch and ordered delivery
- Should have support for process management
  - launching,
  - communicating,
  - destroying
- Should have support for the parallel processing of pure functions
  - ultimately, the greatest performance improvements come from parallelism
